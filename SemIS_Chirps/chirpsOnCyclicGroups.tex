\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\usepackage{tikz}
\usepackage{subcaption}




\title{Frames of Chirps on Finite Cyclic Groups}

\author{\IEEEauthorblockN{Michael L. Leyrer (michael.leyrer@tum.de)}
\IEEEauthorblockA{\textit{TU Munich, Arcisstr. 21 80333 Munich, Germany}}

}

\begin{document}

\maketitle

\begin{abstract}
	Chirps are relevant in many signal processing applications. They arise, for example, in radar imaging from the Doppler effect or in image processing from perspective periodicity. However, chirps are non-stationary signals which renders the discrete Fourier transform (DFT) less well suited for processing them compared to applications with stationary signals. The present paper introduces discrete chirps, the concept of frames, and their connection to the discrete chirp-Fourier transform~(DCFT).
\end{abstract}

\begin{IEEEkeywords}
Discrete chirps, tight frames, chirp transform.
\end{IEEEkeywords}

\section{Introduction}\label{s:intro}

A \textit{linear chirp} is a signal whose frequency varies linearly over time. While there are also other types like the exponential chirp, only linear chirps are considered in this article and are thus referred to as ''chirps'' without specification. Chirps arise, for example, in radar applications with moving components due to the Doppler effect. Extracting chirp rate information from a signal is especially important in syntheic aperture radar~(SAR) where a small transciever moves across a scene to emulate a larger aperture thus yielding better resolution \cite{maISAR,su_fang_2012}. Chirps also appear in image processing when an object with periodic structure that is tilted in perspective is considered. Chirp modulation can be used for digital signaling and exhibits properties superior to schemes like frequency shift keying~(FSK) or phase shift keying~(PSK) in some cases \cite{berniChirpMod}.\\

The DFT is not well suited for analyzing chirp signals. A pure chirp exhibits a broad DFT spectrum, making it difficult to extract information, e.g., about the chirp rate of the signal.
In order to efficiently analyze chirp signals, the DCFT \cite{xia} can be employed. Unlike the DFT, it transforms a signal from one dimensional time domain to two dimensional frequency-chirp rate domain. The DCFT of a chirp - much like the DFT of a pure tone - exhibits a distinct peak at the corresponding chirp rate and frequency value. The DCFT ''basis'' forms a \textit{tight frame} for the time domain signal. This fact is used to derive some of the properties of the transform \cite{main}. The discrete linear chirp transform~(DLCT) \cite{alkishriwoDiss} is a generalization of the DCFT which proves more powerful in most applications at the cost of losing some useful properies.\\

This paper is organized as follows. In Section~\ref{s:chirps}, discrete chirps are defined. Section~\ref{s:frames} introduces the concept of frames and Section~\ref{s:modChirpFrame} explores the frame of modulated chirps which is closely related to the DCFT.\\
%Finally, Section~\ref{s:conc} concludes the paper.\\

\section{Chirps on Finite Cyclic Groups}\label{s:chirps}

In general, a linear, time contiuous chirp is given as
\begin{equation}
	\tilde{c}_{k,l}: \mathbb{R}\to\mathbb{C}:\tilde{c}_{\tilde{k},\tilde{l}}(t)=\exp\left(j2\pi\left(\tilde{k}t+\frac{\tilde{l}t^2}{2}\right)\right),\label{eq:contChirp}
\end{equation}
with frequency offset $\tilde{k}\in\mathbb{R}$ and chirp rate $\tilde{l}\in\mathbb{R}$. The \textit{instantaneous frequency} is defined as the first time derivative of the phase and is thus $f_{\text{inst}}=k+lt$.\\

While the above definition of the time continuous chirp is generally accepted as canonical, there are different ways to define the time discrete chirp. Throughout this paper, the following definition from \cite{main} will be used for time discrete chirps.
\begin{align}
	&c_N^l: \mathbb{Z}_N\to\mathbb{C},\quad l\in\mathbb{Z}_N\nonumber\\
	&c_N^l(t)=w_N^{lt(t-N)/2}=(-1)^{lt}w_N^{lt^2/2}\label{eq:chirpDef}
\end{align}
Here, $\mathbb{Z}_N=\{n\in\mathbb{Z}|0\leq n<N\}$ together with addition modulo $N$ is the the \textit{finite cyclic group} of order $N$ and $w_N=\exp(j2\pi/N)$ is the first complex $N$-root of unity. $l$ is referred to as the \textit{chirp order} which is the discrete equivalent of the chirp rate. Note, that this is a pure chirp with no additional constant frequency term. For simpler notation when modifying the frequency offset, the \textit{wave functions}
\begin{equation}
	m_N^k:\mathbb{Z}_N\to\mathbb{C}: m_N^k(t)=w_N^{kt},\quad k\in\mathbb{Z}_N\label{eq:waveDef}
\end{equation}
are used. They represent all pure tones that are possible in the space of complex valued sequences of length $N$ which is denoted as $\ell(\mathbb{Z}_N)$, i.e., they form the \textit{Fourier basis} of $\ell(\mathbb{Z}_N)$.\\

To justify this seemingly complicated choice for the definition of the discrete chirp, some alternatives are analyzed. In \cite{xia}, $f(t)=w_N^{lt^2}$ is used. This yields a well defined chirp over $\mathbb{Z}_N$, i.e., it fulfills $f(t)=f(t+N)$ for all $t\in\mathbb{Z}$. However, one might argue that this definition does not include the lowest order non-trivial chirp, since the factor of $1/2$ in the $t^2$ term that is found in the continuous version, is missing. Simply using $g(t)=w_N^{lt^2/2}$ is not advisable since it only yields a well defined chirp if $2\vert N$. The shift in the quadratic term in \eqref{eq:chirpDef} compensates this, such that $c_N^l(t)=c_N^l(t+N)$ holds for all $N\in\mathbb{N}$ and $t,l\in\mathbb{Z}_N$, and the chirp is well defined on $\mathbb{Z}_N$.\\

\section{Frames}\label{s:frames}

Frames are a construct that generalize the concept of a basis and are useful in many applications. In the following, they are defined and their properties inspected.\\

\subsection{Definition and Properties}\label{s:frames:def}

Consider a Hilbert space $\mathcal{H}$ with scalar product $\langle\bullet,\bullet\rangle$, induced norm $||f||^2=\langle f,f\rangle$, and a sequence $(f_n)_{n\in\mathcal{I}}\subseteq\mathcal{H}$ with a countable index set $\mathcal{I}$. This sequence is called a \textit{frame} of $\mathcal{H}$, if there exist constants $0<A\leq B<\infty$, called frame bounds, such that for all $f\in\mathcal{H}$
\begin{equation}
	A||f||_2^2\leq\sum_{n\in\mathcal{I}} \left|\langle f,f_n \rangle \right|^2\leq B||f||_2^2.
\end{equation}
There are three operators ususally associated with frames. The \textit{frame analysis operator} $T$, the \textit{frame synthesis operator} $T^*$ and the \textit{frame operator} $S = T^*T$. 
\begin{align}
	&T: {\mathcal{H}}\to\ell({\mathcal{I}}): Tf = (\langle f,f_n \rangle)_{n\in{\mathcal{I}}}\label{eq:analOp}\\
	&T^*: \ell({\mathcal{I}})\to{\mathcal{H}}: T^*(c_n)_{n\in{\mathcal{I}}} = \sum_{n\in{\mathcal{I}}}c_nf_n\label{eq:synthOp}
\end{align}
The following properties are used to characterize frames. If $A=B$, i.e. the lower and upper frame bound are equal, the frame is $A$-\textit{tight} and $f=\frac{1}{A}Sf$ holds. If $||f_n||_2=1$ for all $n\in\mathcal{I}$, it is called \textit{normalized}. Sometimes, this is also referred to as the frame being a unit-norm frame to avoid confusion with the case $A=B=1$. If there is a constant $C$ such that $\left| \langle f_n,f_k\rangle \right| = C$ for all $n\neq k$, it is \textit{equiangular}. In cases where ${\mathcal{H}}$ is a vector space that can be visualized, e.g., ${\mathcal{H}}=\mathbb{R}^3$, this actually means that the angle between any two frame elements is the same as between any two other.\\

A normalized tight frame~(NTF) with frame bound $A$ is referred to as an $A$-NTF. NTFs are a generalization of orthonormal bases (ONB) that allow for overcompleteness, i.e., an element of ${\mathcal{H}}$ can be decomposed as different linear combinations of the frame elements. In fact any ONB is a $1$-NTF and vice versa.\\

\subsection{Redundancy and Maximum Frame Correlation}\label{s:frames:red}

Let $N=\dim({\mathcal{H}})$ denote the dimension of the considered Hilbert space and $m=|{\mathcal{I}}|$ the number of elements in the frame $F$. First note that $F$ being a frame implies $m\geq N$. For $A>0$, the elements of $F$ need to span ${\mathcal{H}}$ and this requires at least $N$ elements. Since $F$ can consist of more than $N$ elements, it is useful to define the \textit{redundancy}
\begin{equation}
	\rho = \frac{m}{N}
\end{equation}
of the frame. Larger redundancy can yield more efficient and robust decompositions $Tf$ of elements $f\in{\mathcal{H}}$ which is desireable in signal processing. However, increased redundancy inevitably leads to more cross-correlation between the frame elements, impairing the performance of the frame in many applications. The degree of cross-correlation is quanitzed by the \textit{maximum frame correlation}
\begin{equation}
	\mathcal{M}(F)=\max_{f_n,f_k\in{F}, f_n\neq f_k}\frac{\left| \langle f_n,f_k \rangle \right|}{||f_n||||f_k||}.\label{eq:maxFrameCorr}
\end{equation}
Frames that minimize the maximum frame correlation are referred to as \textit{Grassmanian} frames. The minimum possible value of ${\mathcal{M}}(F)$ depends both on the redundancy $\rho$ and on the dimension $N$ of the underlying Hilbert space. It is lower bounded by the \textit{Welch bounds} \cite{welch}
\begin{align}
	\mathcal{M}(F)&\geq\left(\frac{1}{m-1}\left(\frac{m}{\begin{pmatrix}
			N+k-1\\k
	\end{pmatrix}}-1\right)\right)^{1/2k}\nonumber\\&=: {\text{WB}}(m,N,k)\label{eq:WB},
\end{align}
which hold for all $k\in\mathbb{N}$. In particular, for the case that $k=1$, \eqref{eq:WB} simplifies to
\begin{equation}
	\mathcal{M}(F)\geq\sqrt{\frac{m-N}{N(m-1)}} =\sqrt{\frac{\rho-1}{N\rho-1}}.\label{eq:WB1}
\end{equation}
Equality in \eqref{eq:WB1} holds if and only if $F$ is equiangular and tight.\\

%\subsection{Examples of Frames}\label{s:frames:ex}

\section{The Modulated Chirp Frame}\label{s:modChirpFrame}
In the following, the frame of normalized modulated chirps
\begin{equation}
	F_c=(m_N^kc_N^l/\sqrt{N})_{(k,l)\in \mathbb{Z}_N^2}\label{eq:modChirpFrame}
\end{equation}
over $\mathbb{Z}_N$ and its properties are considered. Results of this section are mainly based on \cite{main}.\\

\subsection{Basic Properties}\label{s:modChirpFrame:prop}
First note that $F_c$ is normalized. It is also a tight frame by the following argument. For any fixed $l$ and all $f\in\ell(\mathbb{Z}_N)$, it holds that
\begin{align}
	c_N^{-l}f = \sum_{k\in\mathbb{Z}_N}\langle c_N^{-l}f, m_N^k/\sqrt{N}\rangle m_N^k/\sqrt{N} \nonumber\\
	f = \sum_{k\in\mathbb{Z}_N}\langle f, m_N^kc_N^l/\sqrt{N}\rangle m_N^kc_N^l/\sqrt{N}, \label{eq:proofTight1}
\end{align}
because $(m_N^k/\sqrt{N})_{k\in\mathbb{Z}_N}$ constitutes the normalized Fourier basis of $\ell(\mathbb{Z}_N)$, i.e., is an ONB, and because $c_N^{-l}=(c_N^{l})^*$. Taking the sum over all $l\in\mathbb{Z}_N$ yields
\begin{equation}
	f = \frac{1}{N}\sum_{(k,l)\in\mathbb{Z}_N^2}\langle f, m_N^kc_N^l/\sqrt{N}\rangle m_N^kc_N^l/\sqrt{N}.\label{eq:proofTight2}
\end{equation}
Thus, $F_c$ is an $N$-NTF of $\ell(\mathbb{Z}_N)$.\\

However, $F_c$ is not equiangular since there exist $k\neq k_0$ and $l\neq l_0$ such that
\begin{equation}
	\langle m_N^{k_0}c_N^{l_0},m_N^{k}c_N^{l_0} \rangle = 0 \neq \sqrt{\frac{1}{N}} \leq \langle m_N^{k_0}c_N^{l_0},m_N^{k}c_N^{l} \rangle.
\end{equation}
The inequality on the right is closely related to the maximum frame correlation of $F_c$ and is discussed in Section~\ref{s:modChirpFrame:frameCorr}.\\

\subsection{Connection to DCFT}\label{s:modChirpFrame:DCFT}

The frame analysis operator $T_c: \ell(\mathbb{Z}_N)\to\ell(\mathbb{Z}_N^2)$ of the normalized modulated chirp frame $F_c$ exhibits close similarity to the DCFT introduced by Xia \cite{xia}.
\begin{equation}
	[{\text{DCFT}}(f)](k,l)= \frac{1}{\sqrt{N}}\sum_{t\in\mathbb{Z}_N}f(t)w_n^{-kt-lt^2}\label{eq:DCFT}
\end{equation}
\begin{align}
	(T_cf)(k,l) &=\langle f, m_N^kc_N^l/\sqrt{N}\rangle\nonumber\\&= \frac{1}{\sqrt{N}}\sum_{t\in\mathbb{Z}_N}f(t)m_N^{-k}(t)c_N^{-l}(t)\label{eq:MCF_anaOp}
\end{align}
In fact, for odd $N$, the following relation can be derived.
\begin{equation}
	(T_cf)(k,l) = [{\text{DCFT}}(f)]\left(k-\frac{lN}{2},\frac{l}{2}\right)%,\quad {\text{if}}\quad 2\nmid N
\end{equation}
The condition that $N$ is odd is required since $a\in\mathbb{Z}_N$ has a multiplicative inverse $a^{-1}$ in the ring $(\mathbb{Z}_N,+,\cdot)$ if and only if $a$ and $N$ are co-prime, i.e., $\gcd(a,N)=1$. Thus $l/2$ only exists for all $l\in\mathbb{Z}_N$ if $N$ is odd.\\

\subsection{Maximum Frame Correlation}\label{s:modChirpFrame:frameCorr}

Due to the above connection between $F_c$ and the DCFT, there is a direct relation between the maximum frame correlation ${\mathcal{M}}(F_c)$ and the maximum sidelobe amplitude of the DCFT of a single normalized modulated chirp. The two are in fact equal. This is one example where large maximum frame correlation is undesireable because higher sidelobe amplitudes lead to more uncertainty and larger error probabilities when analyzing signals, especially in the presence of noise. The following expression for the maximum frame correlation is derived by Casazza \cite{main}.
\begin{align}
	{\mathcal{M}(F_c)} = \sqrt{\frac{N_1}{N}},\quad N_1=\max_{n|N,n<N}n,
\end{align}
where $N_1$ is the greatest proper divisor of $N$. The normalized modulated chirp frame thus performs best when $N$ is prime. This was also pointed out by Xia \cite{xia}.\\

In the following, only cases where $N$ is prime are considered and the maximum frame correlation is compared to the first Welch bound \eqref{eq:WB1}. The number of elements in $F_c$ is $m_c = |\mathbb{Z}_N^2|=N^2$ and therefore the redundancy is $\rho_c = N$. The first Welch bound evaluates to
\begin{align}
	{\text{WB}}(N^2,N,1) = \sqrt{\frac{N-1}{N^2-1}}=\sqrt{\frac{1}{N+1}}.
\end{align}
Since it is not an equiangular frame, $F_c$ does not achive Welch bound equality. However, for large primes $N$, its maximum frame correlation converges to the first Welch bound.\\

The DLCT \cite{alkishriwoDiss}, which is a generalization of the DCFT for non-integer chirp ''orders'', is the preferred choice for analyzing practical chirp signals with chirp rates that not integer multiples of the square of the sampling frequency. For integer chirp orders, however, it also features larger sidelobes than the DCFT.\\

\section{Conclusion}\label{s:conc}

Discrete Chirps, especially as the frame of modulated chirps, are useful in signal processing applications with non-stationary signals. The modulated chirp frame performs best when the number of sampling points is a large prime number as this leads to minimal maximum frame correlation, i.e., to minimal sidelobe amplitudes when analyzing the signal using the DCFT. If the signal has non-integer chirp rates with the chosen sampling rate, this property is lost and the DLCT is preferred over the DCFT. 


\bibliographystyle{IEEEtran}
\bibliography{chirpsOnCyclicGroups}

\end{document}
