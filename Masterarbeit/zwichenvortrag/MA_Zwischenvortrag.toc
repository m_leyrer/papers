\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{Idee und Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Grundlagen Pseudo-Zufallsfolgen}{4}{0}{2}
\beamer@sectionintoc {3}{Systemanforderungen}{7}{0}{3}
\beamer@sectionintoc {4}{Effekte von nicht-idealer Signalverarbeitung}{8}{0}{4}
\beamer@sectionintoc {5}{Systemstruktur}{14}{0}{5}
\beamer@sectionintoc {6}{Design}{16}{0}{6}
\beamer@sectionintoc {7}{Ergebnisse}{19}{0}{7}
