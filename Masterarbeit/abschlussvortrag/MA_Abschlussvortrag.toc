\babel@toc {ngerman}{}
\beamer@sectionintoc {1}{Idee und Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Grundlagen Pseudo-Zufallsfolgen}{4}{0}{2}
\beamer@sectionintoc {3}{Systemstruktur}{8}{0}{3}
\beamer@sectionintoc {4}{Systemanforderungen}{8}{0}{4}
\beamer@sectionintoc {5}{Effekte von nicht-idealer Signalverarbeitung}{9}{0}{5}
\beamer@sectionintoc {6}{Design}{17}{0}{6}
