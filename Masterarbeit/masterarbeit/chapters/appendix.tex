\chapter{Single-Ended to Differential Frequency Doubler}

\begin{figure}
	\centering
	\begin{circuitikz}
		\draw (-2,0)node[nmos,arrowmos,bulk,xscale=-1](n1){};
		\draw (2,0)node[pmos,arrowmos,emptycircle,bulk](p1){};
		\draw (0,0)to[short,*-o](0,-.5)node[anchor=north]{$V_{\text{b}}+v_{\text{in}}$}(n1.G)--(p1.G);
		\draw(n1.S)|-(0,-1.5)-|(p1.D)(p1.S)|-(0,1.5)-|(n1.D);
		\draw(0,1.5)to[R,l=$R_{\text{L}}$,i<=$i$,*-o](0,4)node[anchor=east]{$V_0$};
		\draw(0,-4)node[anchor=east]{$-V_0$}to[R,l=$R_{\text{L}}$,o-*](0,-1.5);
		\draw(2,1.5)to[short,*-o](2.5,1.5)node[anchor=west]{$v_2$};
		\draw(2,-1.5)to[short,*-o](2.5,-1.5)node[anchor=west]{$v_1$};
		\draw(n1.D)node[currarrow,rotate=-90]{}node[anchor=east]{$i_{\text{n}}$};
		\draw(p1.D)node[currarrow,rotate=90]{}node[anchor=west]{$i_{\text{p}}$};
	\end{circuitikz}
	\caption{Single-ended to differential frequency doubler schematic.}
	\label{fig:app:doubler:schem}
\end{figure}

This chapter introduces a novel single-ended to differential frequency doubler which is employed in the frequency multiplier chain of an earlier version of the $140\,{\text{GHz}}$ transmitter. It combines both an active balun and a frequency doubler in a single, simple circuit (see Figure~\ref{fig:app:doubler:schem}).\\

This circuit employs NMOS and PMOS devices in parallel to each other with a symmetrical load $R_{\text{L}}$ to both positive and negative supply rails. The input signal $v_{\text{in}}$ with a DC bias $V_{\text{b}}$ is applied to both MOS gates, while the differential output voltage is the voltage across the MOS channels $v_2-v_1$.\\


Assuming operation in the weak-inversion region, the drain currents are given as
\begin{align}
	i_{\text{n}} = I_{{\text{n},0}}\exp\left(\frac{V_{\text{b}}+v_{\text{in}}-v_1}{nV_{\text{T}}}\right),\\
	i_{\text{p}} = I_{{\text{p},0}}\exp\left(\frac{-V_{\text{b}}-v_{\text{in}}+v_2}{nV_{\text{T}}}\right),
\end{align}
for the NMOS device and PMOS device, respectively. The influence of the drain-source voltage is neglected to simplify calculations and because it does not change the behavior of the circuit qualitatively.


Together with
\begin{align}
	i=i_{\text{n}}-i_{\text{p}}
\end{align}
and
\begin{align}	
	v_1 = -V_0+R_{\text{L}}i=-v_2	
\end{align}
this yields a transcendental equation for the total current:
\begin{align}
	i &= I_{{\text{n},0}}\exp\left(\frac{V_{\text{b}}+v_{\text{in}}+V_0-R_{\text{L}}i}{nV_{\text{T}}}\right)\nonumber\\&- I_{{\text{p},0}}\exp\left(\frac{-V_{\text{b}}-v_{\text{in}}+V_0-R_{\text{L}}i}{nV_{\text{T}}}\right)
\end{align}
The solution for $i$ can be written in closed form using the principal branch $W_0$ of the \textit{Lambert} $W$ \textit{function} \cite{lambertw}.
\begin{align}
	i = \frac{nV_{\text{T}}}{R_{\text{L}}}W_0\Bigg(&
	\frac{I_{{\text{n},0}}R_{\text{L}}}{nV_{\text{T}}}\exp\left(\frac{V_{\text{b}}+v_{\text{in}}+V_0}{nV_{\text{T}}}\right)\nonumber\\&- \frac{I_{{\text{p},0}}R_{\text{L}}}{nV_{\text{T}}}\exp\left(\frac{-V_{\text{b}}-v_{\text{in}}+V_0}{nV_{\text{T}}}\right)
	\Bigg)
\end{align}

By substituting
\begin{align}
	I_0 &= \frac{nV_{\text{T}}}{R_{\text{L}}},\\
	\alpha_{\text{n}}&=2\frac{I_{{\text{n},0}}R_{\text{L}}}{nV_{\text{T}}}\exp\left(\frac{V_0+V_{\text{b}}}{nV_{\text{T}}}\right),\\
	\alpha_{\text{p}}&=-2\frac{I_{{\text{p},0}}R_{\text{L}}}{nV_{\text{T}}}\exp\left(\frac{V_0-V_{\text{b}}}{nV_{\text{T}}}\right),
\end{align}
the following simplified form is obtained:
\begin{align}
	i=I_0W_0\left(\frac{\alpha_{\text{n}}}{2}e^{\frac{v_{\text{in}}}{nV_{\text{T}}}}+\frac{\alpha_{\text{p}}}{2}e^{\frac{-v_{\text{in}}}{nV_{\text{T}}}}\right)
\end{align}
By choosing $V_{\text{b}}$ such that $\alpha_{\text{n}}=\alpha_{\text{p}}=\alpha$, this can be further simplified to
\begin{align}
	i=I_0W_0\left(\alpha\cosh\left(\frac{v_{\text{in}}}{nV_{\text{T}}}\right)\right).
\end{align}
The output voltage is obtained as
\begin{align}\label{eq:app:doubler:outputVoltage}
	v_{\text{out}}=2V_0-2nV_{\text{T}}W_0\left(\alpha\cosh\left(\frac{v_{\text{in}}}{nV_{\text{T}}}\right)\right).
\end{align}
In order to determine which harmonics are present in the current, if $v_{\text{in}}$ is a sinusoid, a Taylor series of $i$ around $v_{\text{in}}=0$ is useful. The $n$-th order term of the Taylor series produces all harmonics of order $\{n-2k|\frac{n}{2}\geq k\in\mathbb{N}_0\}$. However, as mainly the presence of the first and third harmonic are of importance, it is not necessary to calculate the entire Taylor series, which might not even be possible in closed form. Instead it is sufficient to note that the hyperbolic cosine is an even function, i.e., its Taylor series around $0$ only features even order terms, and that this also holds for any function composition with a hyperbolic cosine, including $i(v_{\text{in}})$ or $v_{\text{out}}(v_{\text{in}})$.\\

Thus, no odd harmonics can be present in $i$, if $\alpha_{\text{n}}=\alpha_{\text{p}}$. In reality and especially at high frequencies, the first harmonic is present in the common mode of $v_1$ and $v_2$ due to capacitive coupling. If there are slight asymmetries in this coupling, the first harmonic additionally infiltrates their differential mode, i.e., the output voltage.\\

Figure~\ref{fig:app:doubler:outputModelVsSim} shows the AC output voltage of the doubler obtained by simulation with $R_{\text{L}}=56\,{\text{k}\Omega}$, $V_0=0.5\,{\text{V}}$, $\frac{w}{l}=\frac{5\,{\text{\textmu m}}}{40\,{\text{nm}}}$, and $V_{\text{b}}$ such that $\alpha_{\text{n}}=\alpha_{\text{p}}$, and the AC output voltage predicted by \eqref{eq:app:doubler:outputVoltage} for the extracted parameters in the operating point $\alpha=5.9$ and $n=2.2$. The input voltage is a $100\,{\text{mV}}$ peak $10\,{\text{MHz}}$ signal and $V_{\text{T}}=26\,{\text{mV}}$ in both cases. The quite large value for $n$ is caused by the proximity to moderate inversion. In the deep sub-threshold regime, $n$ reaches its minimum value of $1.8$.\\

The actual implementation of this doubler features a load resistance of only $1.2\,{\text{k}\Omega}$ to provide enough drive capability at frequencies in the GHz range. At the according transistor operating point, the weak inversion model is no longer valid and instead a moderate inversion model should be used. However, to describe the circuit behavior qualitatively and to understand the mechanism used to cancel the first harmonic, the above model is well suited.\\

%Figure~\ref{fig:app:doubler:outputCurrent} depicts the shape of the output current for $\alpha=1$ and $v_{\text{in}}=nV_{\text{T}}\cos(x)$. By increasing $\alpha$ or the amplitude of $v_{\text{in}}$, the amplitude of $i$ increases. However, at an amplitude of $v_{\text{in}}$ much larger than $nV_{\text{T}}$ additional harmonics become apparent as can be seen in Figure~\ref{fig:app:doubler:outputCurrent2}.

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig/appendix/doubler}{outputVoltage_model_vs_sim.pgf}}
	\caption[Simulated doubler output voltage versus model prediction.]{Simulated doubler output voltage versus model prediction, both with DC offset removed.}
	\label{fig:app:doubler:outputModelVsSim}
\end{figure}

%\begin{figure}
%	\centering
%	\resizebox{\linewidth}{!}{\importpgf{fig/appendix/doubler}{W0_of_cosh_of_cos.pgf}}
%	\caption{Plot of the function $x\mapsto W_0(\cosh(\cos x))$.}
%	\label{fig:app:doubler:outputCurrent}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\resizebox{\linewidth}{!}{\importpgf{fig/appendix/doubler}{W0_of_cosh_of_100cos.pgf}}
%	\caption{Plot of the function $x\mapsto W_0(\cosh(100\cos x))$.}
%	\label{fig:app:doubler:outputCurrent2}
%\end{figure}


%\chapter{Transmission Line Impedance Matching}\label{app:match}
%
%\begin{figure}
%	\centering
%	\begin{circuitikz}
%		\draw(0,2)to[TL,o-,l=${(Z_{\text{L}},l)}$](3,2)to[R,l=$Z$](3,0)node[ground]{};
%		\draw[-latex](-1,1)node[anchor=north]{$Z'$}|-(-.5,2);
%	\end{circuitikz}
%	\caption{Impedance matching using a transmission line.}
%	\label{fig:app:matching}
%\end{figure}
%
%Impedance matching can be achieved in various ways, one of which is by using a transmission line as shown in Figure~\ref{fig:app:matching}. The transformed impedance is given as
%\begin{align}\label{eq:app:match}
%	Z' = Z_{\text{L}}\cdot\frac{Z\cos\beta l + jZ_{\text{L}}\sin\beta l}{Z_{\text{L}}\cos\beta l + jZ\sin\beta l}.
%\end{align}
%
%Calculating the result given $Z$, $Z_{\text{L}}$ and $l$ is a straight forward task. However, retrieving the required TL parameters $Z_{\text{L}}$ and $l$ if $Z$ and $Z'$ are given, is more difficult. While Equation~\eqref{eq:app:match} can be solved numerically, the easier method is graphically using a Smith chart.\\
%
%The transformations from Z-parameters to S-parameters and vice versa are \textit{inversive transformations} which map generalized circles, i.e., circles or infinite lines, to generalized circles \cite{smithChartInvTrans}. Therefore, the transformation path of any impedance by a lossless $(Z_{\text{L}},l)$ TL for $\beta l\in[0,\pi]$, which is a circle in a Smith chart normalized to $Z_{\text{L}}$, is also a circle in a Smith chart normalized to any $Z_{\text{L}}\in\mathbb{R}^+\cdot\Omega$. Note that the center of a transformation circle of any TL with real characteristic impedance $Z_{\text{L}}\in\mathbb{R}^+\cdot\Omega$ always lies on the real axis. This is due to the fact that any impedance $Z$ can be transformed to its complex conjugate $Z^*$ with any $Z_{\text{L}}$.\\
%
%Given $Z$ and $Z'$, $Z_{\text{L}}$ and $l$ can be determined by following these steps:
%
%\begin{enumerate}
%	\item Choose an arbitrary normalization impedance $Z_0$ and mark $Z$ and $Z'$ on the Smith chart.
%	\item Construct the \textit{perpendicular bisector} of the line connecting $Z$ and $Z'$. All points on the perpendicular bisector are at the same distance from both.
%	\item Draw a circle around the intersection point of the perpendicular bisector and the real axis, such that both $Z$ and $Z'$ lie on it.
%	\item Read off the impedance values $Z_1$ and $Z_2$ at which this circle intersects the real axis.
%	\item Choose $Z_{\text{L}}=\sqrt{Z_1Z_2}$.
%	\item Mark $Z$ and $Z'$ normalized to $Z_{\text{L}}$ in a Smith chart and find the clockwise angle $\theta$ from $Z$ to $Z'$ them.
%	\item Choose $l = \frac{\theta}{2\beta}$.\\
%\end{enumerate}
%
%Figure~\ref{fig:app:matching:path} shows steps 1 to 4 exemplary for $Z=(5+{\text{j}}8)\,\Omega$ and $Z'=(20+{\text{j}}35)\,\Omega$ which are marked with blue crosses. These steps are in fact invariant to swapping $Z$ and $Z'$. The dashed blue line represent their perpendicular bisector (step 2) and the black dashed circle is obtained by step 3. Finally, $Z_1$ and $Z_2$ are marked with red crosses, yielding $Z_{\text{L}}=20.6\,\Omega$. Steps 6 and 7 yield $l=0.12\lambda$.\\
%
%If losses occur in the TL, the transformation path is no longer a circle but rather a spiral curling inwards to $Z_{\text{L}}$. This can be accounted for approximately by drawing the circle in step 3 such that $Z'$ lies slightly inside, thereby counteracting the inward spiral.\\
%
%\begin{figure}
%	\centering
%	\resizebox{\linewidth}{!}{\importpgf{fig/appendix}{TL_trans_5+j8to20+j35.pgf}}
%	\caption[Transmission line impedance matching example.]{Transmission line impedance matching example for $Z=(5+{\text{j}}8)\,\Omega$ and $Z'=(20+{\text{j}}35)\,\Omega$.}
%	\label{fig:app:matching:path}
%\end{figure}
