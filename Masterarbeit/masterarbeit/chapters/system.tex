\chapter{System Considerations}\label{c:system}

Before implementing the radar system, some aspects are considered on system level in order to determine the required performance and parameters of the RF front-end and of the analog baseband.\\

Table~\ref{tab:system:giv:fixSysParam} lists given system parameters as well as performance metrics of the already existing transmit RF front-end and on-chip antenna array that are to be employed.\\
%Figure~\ref{fig:system:giv:PTX} further shows the combined output power of the current version of the employed power amplifiers.

\begin{table}
	\centering
	
	\begin{tabular}{|l|c|l|}
		\hline
		{System Parameter}&{Symbol}&{Value}\\\hline
		Center Frequency& $f_{0}$&$140\,{\text{GHz}}$\\
		BB Symbol Rate &$f_{\text{s}}$&$8.8\,{\text{GHz}}$\\
		System Temperature& $T$& {$100\,^{\text{o}}{\text{C}}$}\\
		Transmit Power& $P_{\text{t}}$&{$6\,{\text{dBm}}$}\\
		Transmit Antenna Gain& $G_{\text{t,ant}}$&{$7\,{\text{dBi}}$}\\
		Receive Antenna Gain& $G_{\text{r,ant}}$&{$5\,{\text{dBi}}$}\\\hline		
	\end{tabular}
	\caption{Given radar system parameters.}
	\label{tab:system:giv:fixSysParam}
\end{table}

%\begin{figure}
%	\centering
%	\resizebox{\linewidth}{!}{\importpgf{fig/design}{PA_output_power_into_Zant_HB.pgf}}
%	\caption{Transmitter output power.}
%	\label{fig:system:giv:PTX}
%\end{figure}

\section{Reference Scenario}\label{s:system:scenario}
\begin{figure}
	\centering
	\begin{circuitikz}
		\draw(0,1)--(1,1)node[bareantenna,rotate=-90](tx){};
		\draw(0,-1)--(1,-1)node[bareantenna,rotate=-90](rx){};
		\draw(tx.left)node[anchor=south]{TX} (rx.right)node[anchor=north]{RX};
		\draw(tx.top)node[waves,rotate=0,anchor=left](wtx){};
		\draw(rx.top)+(.2,0)node[waves,rotate=180,anchor=right](wrx){};
		\draw(8,1)circle[radius=.6]{}node(target1){$0.2\,{\text{m}}^2$};
		\draw(5,-1)circle[radius=.6]{}node(target2){$1\,{\text{m}}^2$};
		\draw[|-|](3.25,.5)--(7.25,.5)node[midway,anchor=south]{$3\,{\text{m}}$};
		\draw[|-|](3.25,-1.5)--(4.25,-1.5)node[midway,anchor=south]{$0.6\,{\text{m}}$};
		\draw[-latex](5.6,-1)--(6.4,-1)node[anchor=south]{$|\vec{v}|\leq30\,\frac{{\text{m}}}{{\text{s}}}$};
		\draw[-latex]decorate[decoration=snake]{(.75,.75)--(.75,-.65)}--(.75,-.75);
		\draw(.75,0)node[anchor=west]{$<-14\,{\text{dB}}$};
	\end{circuitikz}
	\caption[Reference scenario for $140\,{\text{GHz}}$ PMCW radar system.]{Reference scenario for $140\,{\text{GHz}}$ PMCW radar system.}
	\label{fig:refScen}
\end{figure}

%\begin{table}
%	\centering
%	\caption{Variable radar system parameters.}
%	\label{tab:varSysParam}
%	\begin{tabular}{|l|c|}
%		\hline
%		{System Parameter}&{Symbol}\\\hline
%		Sequence Length& $N_{\text{seq}}$\\
%		Internal Receiver Gain& $G_{\text{r,int}}$\\
%		Receiver Noise Figure& ${\text{NF}}$\\
%		Detection Threshold& $P_{\text{min}}$\\  \hline
%		
%	\end{tabular}
%\end{table}

In the following, a reference scenario is introduced from which requirements for the $140\,{\text{GHz}}$ radar system are derived. The \textit{radar equation} is used to determine the power of a signal that is reflected by a target and received:
\begin{align}\label{eq:radarEq}
	P_{\text{r}} = P_{\text{t}}\frac{G_{\text{t,ant}}G_{\text{r,ant}}\lambda^2\sigma}{(4\pi)^3r^4} = P_{\text{t}}\frac{1}{\alpha},
\end{align}
where $r$, $\sigma$, and $\lambda$ are the range, \textit{radar cross-section}~(RCS), and wavelength, respectively.\\

The radar transceiver should be able to detect a target with a RCS of $0.2\,{\text{m}^2}$ at a range of $3\,{\text{m}}$, e.g., to detect human presence. According to Equation~\eqref{eq:radarEq} and using the parameters from Table~\ref{tab:system:giv:fixSysParam}, such a target yields receive signal power of {$-95\,{\text{dBm}}$}. Apart from noise and transmissions from other RF devices in vicinity, there are two types of interfering signals that are both shown in Figure~\ref{fig:refScen} together with the primary target.\\

The first type is called TX-RX-leakage and it emerges due to the proximity of --~and thus bad isolation between --~the transmit and receive antennas which are both located on the same chip just {$1.19\,{\text{mm}}$} apart. The isolation was found to be at worst $-14\,{\text{dB}}$ by simulating the antennas with their respective combining and matching networks. This yields an interfering signal with at most $-8\,{\text{dBm}}$ and constant phase.\\

The second type are strong signals with non-constant phase caused by moving targets that have a large RCS and are close to the system. The extreme case is assumed to be a $1\,{\text{m}}^2$ target at a range of $0.6\,{\text{m}}$ moving radially at a velocity of at most $30\,\frac{{\text{m}}}{{\text{s}}}$. This results in a signal power of $-59\,{\text{dBm}}$ and a difference frequency
\begin{align}
	\Delta f = f_{0}\frac{2v}{c} \leq 28\,{\text{kHz}}.
\end{align}


\begin{table}
	\centering

	\begin{tabular}{|l|c|l|}
		\hline
		{Source}&{Symbol}&{Value}\\\hline
		Primary Signal&$P_{\text{s}}$&$-95\,{\text{dBm}}$\\
		TX-RX-leakage&$P_{\text{leak}}$&$-8\,{\text{dBm}}$\\
		Moving Target&$P_{\text{mov}}$&$-59\,{\text{dBm}}$\\ \hline
		
	\end{tabular}
	\caption{Receiver input signal levels.}
	\label{tab:rxSig}
\end{table}

\section{Requirement Analysis}\label{s:system:req}

In the following sections, requirements for the radar system are derived based on the reference scenario in Figure~\ref{fig:refScen} such that the primary target with a signal level of $-95\,{\text{dBm}}$ can be detected reliably.\\

\subsection{Noise Figure}\label{ss:system:req:nf}
At a noise temperature of $100\,^{\text{o}}{\text{C}}$ and with a noise BW of $20\,{\text{GHz}}$, the available thermal noise power $P_{\text{n}}$ is $-70\,{\text{dBm}}$. While the actual input noise temperature might differ, $100\,^{\text{o}}{\text{C}}$ was chosen because all circuits are designed for this operating temperature. This way, the worst case performance is accounted for. Together with the previously determined primary signal level of $-95\,{\text{dBm}}$ this yields
${\text{SNR}_{\text{s}}}=P_{\text{s}}/P_{\text{n}}=-25\,{\text{dB}}$.
The system should be able to detect a target with a $5\sigma$ certainty which requires a signal amplitude of $10\sigma_{\text{n}}$ and a detection threshold of $5\sigma_{\text{n}}$, where $\sigma_{\text{n}}$ is the \textit{root-mean-square}~(RMS) noise amplitude. This way, the noise must reach an amplitude of $5\sigma_{\text{n}}$ to either imitate a target where there is none or to conceal a target by pushing the signal below the detection threshold. A $10\sigma_{\text{n}}$ signal amplitude translates to a SNR of $20\,{\text{dB}}$. The overall noise figure of the receiver must therefore be less than $-45\,{\text{dB}}$.\\

The gain in SNR obtained by the correlation procedure can be approximated by the noise figure
\begin{align}\label{eq:procNF}
	{\text{NF}}_{\text{corr}} \approx (2\pi B_{\text{n}}T_{\text{int}})^{-1},
\end{align}
where $B_{\text{n}}$ and $T_{\text{int}}$ are the noise bandwidth and integration time, respectively. This is due to the fact, that integrating over a signal for the duration $T_{\text{int}}$ is --~regarding the amplitude of the transfer characteristics --~very similar to low-pass filtering it with a cut-off frequency of $({2\pi T_{\text{int}}})^{-1}$ which is illustrated in Figure~\ref{fig:lpfVsInt}. ${\text{NF}}_{\text{corr}}$ is thus the factor by which the noise bandwidth and therefore the noise power is reduced. This does not affect the signal power since after correlating, the signal frequency is at most what is caused by the Doppler effect which is typically much smaller than the integration cut-off frequency. Therefore, ${\text{SNR}}_{\text{s}}$ is increased.\\

Assuming a $17$-bit PRBS that has total duration of $(2^{17}-1)/f_{\text{s}}=14.8\,{\text{\textmu s}}$ and is correlated over one sequence duration, results in ${\text{NF}}_{\text{corr}}=-63\,{\text{dB}}$. This leaves $18\,{\text{dB}}$ noise figure to spare for the receiver signal chain. The LFSR size choice of $17$ bit is justified in Section~\ref{ss:system:dyn:acfBase}.\\

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig}{LPFvsInt_2pi.pgf}}
	\caption[Transfer characteristics of an integration over a finite period of time compared to a first order low-pass filter.]{Transfer characteristics $H$ of an integration over a finite period of time compared to a first order low-pass filter.}
	\label{fig:lpfVsInt}
\end{figure}

\subsection{Bandwidth and In-Band Behavior}\label{ss:system:req:bw}
Since the range resolution of a radar system is proportional to the bandwidth of the employed signal, any reduction in signal bandwidth leads to worse resolution. Therefore, all relevant components of the radar system must be able to support the bandwidth of the signal.\\

The bandwidth of the entire signal path should not degrade the range resolution significantly. Figure~\ref{fig:PRBS_ccf_LPF:a} illustrates the effects of applying a \textit{low-pass filter}~(LPF) to the BB signal for a $7$-bit PRBS. Whilst the case for $f_{\text{c}}=5\,{\text{GHz}}$ might not appear to be critical due to the rather small peak height of $127$, choosing a longer sequence drastically increases the width of the portion of the peak that may exceed the detection threshold. Figure~\ref{fig:PRBS_ccf_LPF:b} shows an example in which a $15$-bit PRBS is employed. Choosing a detection threshold of $10$ --~which is a reasonable value, as it allows for detection of small targets but is significantly larger than the base-level --~leads to a range resolution degradation of $+85\%$.
The overall two-sided $3\,{\text{dB}}$ bandwidth of the entire signal path should not be lower than around $20\,{\text{GHz}}$ in order to preserve range resolution.\\

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs7bitCcf_lpf.pgf}}
		\caption{$7$-bit PRBS.}
		\label{fig:PRBS_ccf_LPF:a}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs15bitCcf_lpf.pgf}}
		\caption{$15$-bit PRBS.}
		\label{fig:PRBS_ccf_LPF:b}
	\end{subfigure}
	\caption[Cross-correlation function of low-pass filtered received sequence with its reference sequence.]{Cross-correlation function of a low-pass filtered received sequence with its reference sequence for LPF cut-off frequencies $f_{\text{c}}\in\{5\,{\text{GHz}},12\,{\text{GHz}}\}$ together with the reference sequence ACF for comparison.}
	\label{fig:PRBS_ccf_LPF}
\end{figure}

Additionally, the pass-band should feature a relatively smooth transfer function. A notch in the frequency response leads to a deformation of the PRBS CCF. When using a longer sequence, this effect becomes stronger as can be seen in Figure~\ref{fig:PRBS_ccf_BSF}. It was observed that the amplitude of theses deformations is determined by the total amount of power missing from the spectrum, while their damping speed is proportional to the notch bandwidth. The small ripples on the CCF base-level are caused by numerical rounding errors.\\


\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs7bitCcf_rf_bsf.pgf}}
		\caption{$7$-bit PRBS}
		\label{fig:PRBS_ccf_BSF:a}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs15bitCcf_rf_bsf.pgf}}
		\caption{$15$-bit PRBS}
		\label{fig:PRBS_ccf_BSF:b}
	\end{subfigure}
	\caption[Cross-correlation function of RF band-stop filtered received sequence with its reference sequence.]{Cross-correlation function of received sequence that has been band-stop filtered before being downconverted, with its reference sequence. The employed band-stop characteristic is shown in Figure~\ref{fig:BSF144G}.}
	\label{fig:PRBS_ccf_BSF}
\end{figure}

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig}{bandStopFilter_144GHz.pgf}}
	\caption[Band-stop filter transfer function.]{Transfer characteristics of a $144\,{\text{GHz}}$ band-stop filter.}
	\label{fig:BSF144G}
\end{figure}

\subsection{Dynamic Range}\label{ss:system:req:dr}

The required dynamic range of the PMCW radar system can be directly extracted from the introduced reference scenario. The lower bound is given by the minimum detectable signal level $-95\,{\text{dBm}}$, while the upper bound is given by TX-RX-leakage at $-8\,{\text{dBm}}$ for constant phase signals and $-59\,{\text{dB}}$ for varying phase signals. The receiver noise figure requirement was chosen such that the lower bound is fulfilled, while the upper bound of the dynamic range is limited by several factors which are discussed in the following section.\\

\begin{table}
	\centering
	
	\begin{tabular}{|l|c|l|}
		\hline
		{System Parameter}&{Symbol}&Value\\\hline
		%		Internal Receiver Gain& $G_{\text{r,int}}$&\\
		Receiver Noise Figure& ${\text{NF}}$&$\leq18\,{\text{dB}}$\\
		Two-Sided RF Bandwidth&$B_{3\,\text{dB}}$&$\geq20\,{\text{GHz}}$\\ 
		Dynamic Range (stationary)&$[P_{\text{s,min}},P_{\text{s,max}}]$&$[-95\,{\text{dBm}},-8\,{\text{dBm}}]$\\
		Dynamic Range (moving)&$[P_{\text{mov,min}},P_{\text{mov,max}}]$&$[-95\,{\text{dBm}},-59\,{\text{dBm}}]$\\\hline
		
	\end{tabular}
	\caption{Required radar system parameters.}
	\label{tab:reqSysParam}
\end{table}

\section{Dynamic Range Limiting Factors}\label{s:system:dyn}


\subsection{Receiver Linearity}

Fortunately, inter-modulation products do not impair the performance of the PMCW radar system. However, the presence of TX-RX-leakage must not lead to \textit{desensitization} of the receiver. Desensitization occurs when a strong signal saturates a component of the signal chain such that its gain drops significantly. This is characterized by the $1\,{\text{dB}}$ \textit{compression point} $P_{1\,{\text{dB}}}$ which is the input power level at which gain has dropped by $1\,{\text{dB}}$ compared to the small signal gain.
However, the small signal gain at the compression point, i.e., the slope of the output power versus input power (on a linear scale!), may drop by a significantly larger amount than $1\,{\text{dB}}$ depending on the exact characteristic --~theoretically even to $0$. An example is shown in Figure~\ref{fig:p1db}. A hyperbolic tangent transfer characteristic features a small signal gain drop of $2.7\,{\text{dB}}$ at the $1\,{\text{dB}}$ compression point while a hard saturation characteristic has no more small signal gain at all.\\

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig}{gainCompression.pgf}}
	\caption[Illustration of the $1\,{\text{dB}}$ compression point.]{Illustration of the $1\,{\text{dB}}$ compression point. The small signal gain at the compression point may vary drastically.}
	\label{fig:p1db}
\end{figure}

It is therefore safer to stay below the compression point by at least $5\,{\text{dB}}$ where the small signal gain is mostly unaffected. Thus, $P_{1\,\text{dB}}$ of the receiver chain must be at least $-3\,{\text{dBm}}$.\\


\subsection{ACF Base Level}\label{ss:system:dyn:acfBase}

As introduced in Section~\ref{s:princ:prbs}, the ACF of a $m$-bit PRBS features peaks with an amplitude and period of $N=2^m-1$, but also a small base-level of $-1$ in between the peaks. A strong input signal might cause this base-level to be incorrectly detected as a peak. To achieve a robust system with $5\sigma$ detection certainty it is necessary that the CCF base-level introduced by TX-RX-leakage is small compared to CCF peaks of the main target's signal, i.e., one order of magnitude lower in amplitude. The required peak to base-level ratio is thus in the range of
\begin{align}
	P_{\text{leak}}-P_{\text{s}}+20\,{\text{dB}}=107\,{\text{dB}}.
\end{align}

This ratio only depends on the number of bits $m$ of the LFSR that is used to generate the PRBS.

\begin{align}
	\left|\frac{r_x(\tau=kN)}{r_x(\tau\neq kN)}\right|=N\approx m\cdot6\,{\text{dB}},\quad k\in\mathbb{Z}
\end{align}

Employing a $17$-bit PRBS yields a ratio of $102\,{\text{dB}}$ which corresponds to a ratio of $15\,{\text{dB}}$ and is declared as sufficient. The remaining noise margin is approximately $2.8\sigma$ if the detection threshold is adjusted.

\subsection{Varying Phase Signals}\label{ss:system:dyn:varPhase}

Received signals with a shifted frequency, e.g., reflections from a moving target, may also cause issues if the signal phase varies significantly during the correlation procedure. In order to estimate the resulting performance impairment, the following example is examined.\\

Given $f_{0}$ and $f_{\text{s}}$ according to Table~\ref{tab:system:giv:fixSysParam}, a $17$-bit PRBS as determined above, and the target velocity $v=30\,\frac{{\text{m}}}{{\text{s}}}$, the CCF $r_{s,x}$ of the received sequence $s$ and the reference sequence $x$ shown in Figure~\ref{fig:prbsCcfVarPhase} is obtained. While the peak at $\tau=0$ is still present, the base-level no longer has a constant value of $-1$, but appears noise-like and with much larger amplitude. During one sequence period, the target travels a distance
\begin{align}
	\Delta r = \frac{2^{17}-1}{8.8\,{\text{GHz}}}\cdot30\,\frac{{\text{m}}}{s} = 0.45\,{\text{mm}}\approx\frac{1}{4.8}\lambda_{0},
\end{align}
where $\lambda_{0}$ is the wavelength of the RF signal in free space. This travelled distance corresponds to a phase shift of $0.42\cdot2\pi$ during the correlation procedure, which is almost a complete sign flip. Therefore, it is reasonable to assume that increasing the target velocity beyond $30\,\frac{{\text{m}}}{{\text{s}}}$ does not increase the base-level noise amplitude significantly. This assumption is confirmed in Figure~\ref{fig:prbsCcfVarPhase2} by examining a scenario with a much faster target. Note, that the peak at $\tau=0$ is now entirely concealed. However, for a different initial signal phase, it might still be distinguishable.\\

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\centering
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs17bitVarPhase30msCcf.pgf}}
		\caption{$v=30\,\frac{{\text{m}}}{{\text{s}}}$}
		\label{fig:prbsCcfVarPhase}
	\end{subfigure}
	
	\begin{subfigure}{\linewidth}
		\centering
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs17bitVarPhase120msCcf.pgf}}
		\caption{$v=120\,\frac{{\text{m}}}{{\text{s}}}$}
		\label{fig:prbsCcfVarPhase2}
	\end{subfigure}
	\caption[CCF of a received sequence from a moving target and its corresponding reference sequence.]{CCF $r_{s,x}$ of a received sequence $s$ from a target moving at velocity $v$ and its corresponding $17$-bit reference sequence $x$.}
\end{figure}

The observed RMS CCF base-level amplitude is approximately $300$ (or $49.5\,{\text{dB}}$). Again, demanding approximately one order of magnitude between the peak of the primary signal and the RMS base-level amplitude, the upper dynamic range limit for moving targets is 
\begin{align}
	P_{\text{mov,max}}=P_{\text{s}}+17\cdot6\,{\text{dB}}-49.5\,{\text{dB}}-20\,{\text{dB}}=-62.5\,{\text{dBm}},
\end{align}
which is just $3.5\,{\text{dB}}$ lower than $P_{\text{mov,max}}=-59\,{\text{dB}}$ in the reference scenario and also declared as sufficient.\\

In conclusion, fast moving, strong targets as in the reference scenario do not impair the correct detection of weak signals. However, depending on the initial signal phase, targets moving faster than $30\,\frac{{\text{m}}}{{\text{s}}}$ might not be detected.\\

\subsection{Phase Noise}\label{ss:system:dyn:phaseNoise}

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig}{phase_wiener_time_4pi2ps.pgf}}
	\caption{Five realizations $p(t)$ of the Wiener process $\mathsf{p}(t)\sim{\mathcal{N}}\left(0,\frac{4\pi^2}{{\text{s}}}t\right)$.}
	\label{fig:phaseNoiseWienerTime}
\end{figure}

\textit{Phase noise} is a random fluctuation $\mathsf{p}(t)$ in the phase of the LO signal
\begin{align}
	\mathsf{c}(t) = A_{0}\cos\left(2\pi f_{0}t + \mathsf{p}(t)\right),
\end{align}
which can be modeled as a \textit{Wiener process} \cite{phaseNoiseWiener1,phaseNoiseWiener2}, i.e., $\mathsf{p}(t)$ is an integral over white noise. A Wiener process has the properties
\begin{align}
	\mathsf{E}\left[\mathsf{p}(t)\right]&=0,\\
	\mathsf{Var}\left[\mathsf{p}(t_{0}+t)-\mathsf{p}(t_{0})\right]&=\gamma |t|.
\end{align}
The parameter $\gamma$ represents the slope with which the variance of the random process increases linearly over time. Figure~\ref{fig:phaseNoiseWienerTime} exhibits five realizations $p(t)$ of the Wiener process $\mathsf{p}(t)$ with $\gamma=4\pi^2/s$, i.e., the RMS phase deviation after $1\,{\text{s}}$ has passed is $2\pi$.\\

The impact of phase noise on the CCF between the received, downconverted signal and the reference sequence is similar to that of an offset frequency which was discussed in Section~\ref{ss:system:dyn:varPhase}. To reliably detect a target, it is sufficient that the phase drift of the LO signal, that accumulates during the time of flight of the signal, is very likely ($5\sigma$) not more than a $90^{\text{o}}$ turn, i.e., that
\begin{align}\label{eq:system:pn:weak}
	5\sqrt{\gamma \tau_{\text{tof,s}}} < \frac{\pi}{2}.
\end{align}

However, to ensure a low CCF base-level noise, the phase drift of strong signals such as TX-RX-leakage must be extremely small:
\begin{align}\label{eq:system:pn:leak}
	\sqrt{\gamma \tau_{\text{tof,leak}}} \ll \pi
\end{align}
Fortunately, the time of flight of TX-RX-leakage $\tau_{\text{tof,leak}}$ is very small. To further ensure coherence of the TX and RX LO signals, the LO path to the receiver can be artificially delayed by $\tau_{\text{tof,leak}}$ compared to the TX LO signal.\\

Phase noise of an oscillator is typically characterized by the \textit{power spectral density}~(PSD) of the generated signal. The parameter $\gamma$ --~which is important to assess the impact of phase noise on the performance of a PMCW radar --~is given as
\begin{align}
	\gamma = 2\pi B_{3\,\text{dB}},
\end{align} 
where $B_{3\,\text{dB}}$ is the (two-sided) $3\,{\text{dB}}$ bandwidth of the LO signal \cite{phaseNoiseWiener1}.\\

The $3\,{\text{dB}}$ bandwidth of the $140\,{\text{GHz}}$ LO signal generated by a $70\,{\text{GHz}}$ VCO followed by a frequency doubler is $50.8\,{\text{kHz}}$~(see Figure~\ref{fig:system:phaseNoiseVCO}). This is without a PLL. Employing Equation~\eqref{eq:system:pn:weak}, this yields a rage limit of
\begin{align}
	r\leq\frac{\pi c_0}{400B_{3\,\text{dB}}}=46.4\,{\text{m}},
\end{align}
which is considerably larger than the range limit imposed by SNR.\\\

\begin{figure}[h]
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig/system}{VCO_PSD_SSB.pgf}}
	\caption{PSD of the employed $70\,{\text{GHz}}$ VCO after frequency doubling.}
	\label{fig:system:phaseNoiseVCO}
\end{figure}

\subsection{Correlation Procedure Fluctuations}

Another major issue is the correlation procedure itself. In a real system, the value of a CCF for a given time shift cannot be instantly evaluated because, at any given moment, only the current value of any signal is present. Employing dynamic elements, such as LPFs, allows to store information for a certain period of time. A block diagram of a correlator with an additional block $H$ is shown in Figure~\ref{fig:correlator}. The output signal is given as
\begin{align}
	y(t) = \int_{0}^{t}[(x\cdot s)\ast h](t'){\text{d}}t',
\end{align}
where $s$, $x$, and $h$ are the input signal, reference sequence, and impulse response ${\mathcal{F}}^{-1}(H)$ of the additional block, respectively, and $(x\cdot s)\ast h$ denotes the convolution of the product $x\cdot s$ with $h$. All signals are assumed to be $0$ for $t<0$.\\

\begin{figure}
	\centering
	\begin{circuitikz}
		\draw (0,0) node[mixer](mix){};
		\draw (3,0) node[fourport,scale=0.5](filt){}node{$H$};
		\draw (6,0) node[plain mono amp](amp){$\int{\text{d}}t$};
		\draw[-latex] (-2,0)node[anchor=south]{$s(t)$}--(mix.west);
		\draw[-latex] (0,2)node[anchor=west]{$x(t)$}--(mix.north);
		\draw[-latex] (mix.east)--(filt.west);
		\draw[-latex] (filt.east)--(amp.in)--+(0.36,0);
		\draw[-latex] (amp.east)--+(1,0)node[anchor=south]{$y(t)$};
	\end{circuitikz}
	\caption[Block diagram of a correlator with an additional block $H$.]{Block diagram of a correlator with an additional block $H$.}
	\label{fig:correlator}
\end{figure}

For $H(f)=1$, i.e., a standard correlator block, it holds that
\begin{align}\label{eq:ccfFromCorr}
	r_{s,x}(0) = y(NT_{\text{s}}),
\end{align}
where $N$ and $T_{\text{s}}$ are the PRBS length and symbol duration, respectively, i.e., $NT_{\text{s}}$ is the sequence duration. However, it would be very difficult to sample the CCF at precisely one instant of time. This can be simplified by forcing either $s(t>NT_{\text{s}})=0$ or $x(t>NT_{\text{s}})=0$, which yields  
\begin{align}\label{eq:ccfFromCorrExt}
	r_{s,x}(0) = y(t\geq NT_{\text{s}}).
\end{align}

Figure~\ref{fig:corrProc} shows $y$ over $t$ for this case with three different $H(f)$. The signal $s$ is composed of one time shifted copy of the reference PRBS $x$ with amplitude $1$ and another copy of $x$ with no time shift but an amplitude of $10^{-\frac{87}{20}}$. The first copy represents TX-RX-leakage and the second one the primary target's signal. With $H(f)=1$ , the output signal features significant fluctuations for $0\leq t\leq NT_{\text{s}}$, which can lead to the integrator saturating and thereby distorting the final output value. This issue can be mitigated by introducing a LPF $H$ between mixer and integrator. Only the \textit{direct current}~(DC) portion of $x\cdot s$ is of interest for the CCF. It can be either extracted by integration or by applying a LPF with small enough cut-off frequency. By filtering out the \textit{alternating current}~(AC) portion of $x\cdot s$ and then applying a gain stage, the risk of saturating the latter can be reduced. Two examples are shown with a first and second order LPF that have a time constant of $NT_{\text{s}}/2$. Note that these two versions require additional time to settle to the correct final value.\\

For the case where $H(f)\neq 1$ and under the conditions $H(0)=1$, $H$ is stable, and $(x\cdot s)(t>NT_{\text{s}})=0$, Equation~\eqref{eq:ccfFromCorrExt} can be further generalized to
\begin{align}\label{eq:ccfFromCorrExt2}
	r_{s,x}(0) = y(t\to\infty) \approx y\left(t-NT_{\text{s}}\gg 1/2\pi f_{\text{c}}\right),
\end{align}
where $f_{\text{c}}$ is the cut-off frequency of $H$ if it has low-pass characteristic.\\

%Applying a LPF with time constant on the order of the sequence period, here, $N/2f_{\text{s}}$, before the integration part of the correlation significantly reduces fluctuations. The final values are equal for all three variants.

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig}{corrProc17bit85dBx1.pgf}}
	\caption[Output signal of the correlator from Figure~\ref{fig:correlator}.]{Output signal of the correlator from Figure~\ref{fig:correlator} with $17$-bit reference PRBS, matched to primary target and unmatched to TX-RX-leakage, with $f_{\text{c}}=2f_{\text{s}}/2\pi N$.}
	\label{fig:corrProc}
\end{figure}

\section{Base Band Flicker Noise}

As soon as the received signal is downconverted, \textit{flicker noise}, or $1/f$-noise, may become an issue. This can be avoided if a \textit{high-pass filter}~(HPF) with a cut-off frequency in the range of several ${\text{MHz}}$ can be applied in the baseband. However, as shown in Section~\ref{ss:system:req:bw}, limiting the spectrum of the PRBS signal can lead to performance degradation. Therefore, the effects of applying a HPF to the received sequence must be investigated.\\

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs7bitCcf_hpf1MHz.pgf}}
		\caption{$7$-bit PRBS.}
		\label{fig:PRBS_ccf_HPF:a}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
		\resizebox{\linewidth}{!}{\importpgf{fig}{prbs15bitCcf_hpf1MHz.pgf}}
		\caption{$15$-bit PRBS.}
		\label{fig:PRBS_ccf_HPF:b}
	\end{subfigure}
	\caption[Cross-correlation function of high-pass filtered received sequence with reference sequence.]{Cross-correlation function of high-pass filtered received sequence with reference sequence for a HPF cut-off frequency of $1\,{\text{MHz}}$.}
	\label{fig:PRBS_ccf_HPF}
\end{figure}

Figure~\ref{fig:PRBS_ccf_HPF} shows two cases. In each of them, a $1\,{\text{MHz}}$, first order HPF is applied to the received sequence before calculating the cross-correlation with the according $8.8\,{\text{GHz}}$ reference sequence. The PRBSs are generated by a $7$-bit LFSR and a $15$-bit LFSR, respectively. While the first one shows almost no performance degradation, i.e, deviation of the base-level from the value of $-1$, the latter features significant impairment on the time bins following the peak. When using longer sequences without lowering the HPF cut-off frequency, this effect becomes worse. This can be explained by examining the spectral components of a PRBS. The fundamental frequency of a PRBS with length $N$ and symbol rate $f_{\text{s}}$ is $f_{\text{s}}/N$. If it is not at least one order of magnitude larger than the HPF cut-off, information is lost. While the information, that is stored in the higher frequency components of a PRBS, seems to affect the shape of the CCF peaks~(see Figure~\ref{fig:PRBS_ccf_LPF}), the lower frequency components seem to determine the shape of the CCF base-level.\\

This is especially critical because the strongest signal, TX-RX-leakage, precedes the signal of any target, thus making correct detection impossible if an HPF is employed to filter out flicker noise. 