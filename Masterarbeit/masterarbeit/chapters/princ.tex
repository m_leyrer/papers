\chapter{Working Principles of a PMCW Radar System}\label{c:princ}
\section{Ranging via Time-of-Flight Measurement}\label{s:princ:ranging}
In order to estimate the distance between the radar and a target, referred to as the \textit{range} $r$, the \textit{time-of-flight} $\tau_{\text{tof}}$ of the signal transmitted to and reflected by the target needs to be measured. This can be achieved in multiple ways by exploiting the structure of the transmitted signal.\\

For example, one of the conceptually simpler radar systems is the pulse radar. This type of radar transmits a short, high-power pulse and directly measures the time-of-flight as the delay time between transmitting a pulse and receiving its echo. A shorter pulse duration, i.e., a larger \textit{bandwidth}~(BW), yields a smaller \textit{range resolution}. The range resolution is of a radar system is the minimum radial distance two targets need to be separated by, such that they are detected as separate targets instead of as a single one.\\

In contrast, in an \textit{frequency modulated continuous wave}~(FMCW) radar, the frequency of a continuously on transmit signal steadily increases (or decreases). This results in a frequency difference between the transmitted and the received signal that is proportional to the time-of-flight. Thus, a range estimate can be obtained by measuring the difference frequency. To achieve this, however, an FMCW radar requires a \textit{phase-locked loop}~(PLL) that can handle an accordingly steep frequency ramp, an \textit{analog-to-digital converter}~(ADC), and digital calculation of the \textit{fast Fourier transform}~(FFT) (see \cite{fmcw80GHz} for an example).\\

A PMCW radar system functions similarly to a pulse radar but it has several advantages at the cost of being more complex. As it is a \textit{continuous wave}~(CW) radar system, the transmit signal is always on. However, the \textit{auto-correlation function}~(ACF) of the down-converted signal is qualitatively the same as the ACF of a single pulse, i.e., it features periodic peaks that correspond to the periodicity of the signal, with a negligible base-level in between. By detecting a peak in the \textit{cross-correlation function}~(CCF) between the transmitted and received signal, the time-of-flight is found to equal the according time shift value of the peak in the CCF minus some constant delay caused by the internal signal path.\\

One of the main advantages of PMCW is that -- similarly to FMCW -- the transmit energy is spread out over a duration much longer than that of a pulse. This way, a better \textit{signal-to-noise ratio}~(SNR) can be achieved as compared to a pulse radar because the BW of the signal is decoupled from the transmit duration. A PMCW radar can thus be driven with much less transmit power which simplifies the  \textit{power amplifier}~(PA) design.\\

In order to a achieve overall good correlation properties and a large bandwidth, the \textit{baseband}~(BB) signal needs to be selected to fulfill these requirements. The following section introduces a class of such signals.

%\section{Range Resolution}\label{s:princ:res}

\section{Pseudo-Random Binary Sequences}\label{s:princ:prbs}

%\subsection{Generating}\label{ss:princ:prbs:gen}

\textit{Pseudo-random binary sequences}~(PRBS) are deterministic and periodic sequences $x(t)$, $t\in\mathbb{Z}_N$, with two possible values (typically $x(t)\in\{-1,1\}$). They can, for example, be generated by employing \textit{linear feedback shift registers}~(LFSR). A generic Galois LFSR, in which the value of the last register is fed back according to the feedback polynomial, is shown in Figure~\ref{fig:lfsr}. An alternative LFSR configuration is the Fibonacci LFSR, in which values of all registers are fed back to the input of the first. The Fibonacci type has a lower fan-out as each register output is used at most twice. However, in most cases it features a longer propagation delay.\\

\begin{figure}
	\centering
	\begin{circuitikz}[scale=.78, transform shape]
		\draw (-1,0)node[flipflop CUS](FF1){};
		\draw (4,0)node[flipflop CUS](FF2){};
		\draw (8,0)node[flipflop CUS](FF3){};
		\draw (2,0)node[adder](A1){};
		\draw (6,0)node[adder](A2){};
		\draw (-3,0)node[adder](A0){};
		\draw (2,1.25)node[buffer,rotate=-90,yscale=.9,xscale=1.05](B1){}(2,1.55)node{$f_{m-2}$};
		\draw (6,1.25)node[buffer,rotate=-90,yscale=.9,xscale=1.05](B2){}(6,1.55)node{$f_{m-1}$};
		\draw (-3,1.25)node[buffer,rotate=-90,yscale=.9,xscale=1.05](B0){}(-3,1.55)node{$f_0$};
		\draw[-latex] (-4.5,0)node[anchor=south]{init}--(A0.west);
		\draw[-latex] (A0.east)--(FF1.west);
		\draw[-latex,dashed](FF1.east)--(A1.west);
		\draw[-latex](A1.east)--(FF2.west);
		\draw[-latex](FF2.east)--(A2.west);
		\draw[-latex](A2.east)--(FF3.west);
		\draw[-latex](FF3.east)--(10,0)node[anchor=south]{out};
		\draw[-latex](9.25,0)|-(6,2.1)--(6,1.75);
		\draw[-latex](6,2.1)-|(2,1.75);
		\draw[-latex](2,2.1)-|(-3,1.75);
		\draw[-latex](B0.east)--(A0.north);
		\draw[-latex](B1.east)--(A1.north);
		\draw[-latex](B2.east)--(A2.north);
	\end{circuitikz}
	\caption[Galois LFSR block diagram.]{Linear feedback shift register in Galois configuration with feedback polynomial $f(x) = \sum_{i = 0}^{m}f_ix^i$ of degree $m$ and with $f_m=1$.}
	\label{fig:lfsr}
\end{figure}

If the feedback polynomial is a \textit{primitive polynomial} of degree $m$ over $\mathbb{Z}_2\cong {\text{GF}(2)}$, each of the $2^m-1$ non-zero states of the LFSR is traversed and a \textit{maximum length sequence}~(MLS) can be generated by observing any single bit of the register. The notion of a primitive polynomial is related to field extensions of finite fields. Given a prime number $p$, a polynomial $f\in {\text{GF}}(p)[x]$ of degree $m$ over the prime field ${\text{GF}}(p)$ is primitive if and only if it is \textit{irreducible}, i.e., it cannot be written as a product of non-trivial factors, and it has a \textit{primitive element} $\alpha$ of ${\text{GF}}(p^{m})$ as a root. A primitive element generates the entire field except for the additive identity $0$, i.e., $GF(p^m)\backslash\{0\}=\{\alpha^k|k\in\mathbb{Z}_{p^m-1}\}$. In the context of LFSRs a clock cycle amounts to multiplying the field element represented by the current register state with the root of the feedback polynomial. Therefore, a primitive polynomial is required in order to traverse every non-zero state and to generate a MLS. A binary MLS which is generated by a $m$-bit LFSR is referred to as a $m$-bit PRBS.\\

%\subsection{Properties}\label{ss:princ:prbs:prop}

Up to periodicity, PRBSs feature correlation properties similar to those of noise, i.e., their ACF $r_x(\tau)$ exhibits a large peak at $\tau=kN$, with $k\in\mathbb{Z}$ and $N$ being the period of the PRBS. For other values of $\tau$, the ACF is almost zero.
%{\color{red}${\text{peak/base}}\propto N_{\text{seq}}$.}
\begin{align}
	r_x(\tau) = \int_{\mathbb{Z}_N} x(t)x(t+\tau){\text{d}}t
\end{align}

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\importpgf{fig}{prbs7bitAcf.pgf}}
	\caption[ACF of a 7-bit PRBS]{ACF of a 7-bit PRBS $x$ with normalized $\tau$. The peaks have amplitude $r_x(\tau=kN)=N=2^7-1$, while the base-level in between is $r_x(\tau\neq kN)=-1$, for $k\in\mathbb{Z}$.}
	\label{fig:prbsAcf}
\end{figure}

Figure~\ref{fig:prbsAcf} shows the ACF of a 7-bit PRBS with values $\pm1$. Its triangular peaks have an amplitude of $2^7-1=127$ as well as a period of $127$, while in between the peaks the ACF features a constant base-level at $-1$. In general, the ACF of any $m$-bit PRBS with amplitude $1$ features peaks heights $N=2^m-1$ and a base-level of $-1$ \cite{prbsCorr}. The base width of the peak is $2T_{\text{s}}$, where $T_{\text{s}}$ is the duration of one symbol.
PRBSs with $f_{\text{s}}=T_{\text{s}}^{-1}$ in the gigahertz range are therefore ideal candidates for the BB signal of a PMCW radar system.\\

%{\color{red}Longer PRBS->smaller side level->higher dynamic range! BUT longer sampling time -> moving targets (10m/s) may have non-constant phase during sampling -> larger cross correlation side ''noise''.There should be an optimal squenece length!!! Maybe not that bad after all, since TX/RX leakage is dominant with constant phase...\\	At 30dB dyn range between targets, the strong one moving at ~30m/s is no problem even with 17bit LFSR!! -> limiting factor is sampling time(15us for 17bit) -> 15bit is enough for 70dB dyn range,  maybe go for 16 -- but more complicated polynomial\\}



\section{System Architecture}

\begin{figure}
	\centering
	\resizebox{\linewidth}{!}{\import{fig}{140GHzQPSKTX+RX_externalControl_color2.eps_tex}}
	\caption{Block diagram of the PMCW radar system.}
	\label{fig:sysBlockDiagram}
\end{figure}

The block diagram of the proposed PMCW radar system prototype is shown in Figure~\ref{fig:sysBlockDiagram}.\\

A $70\,{\text{GHz}}$ \textit{voltage controlled oscillator}~(VCO) and a frequency doubler are used to generate the $140\,{\text{GHz}}$ \textit{local oscillator}~(LO) signal. By dividing the $70\,{\text{GHz}}$ signal by a factor of $8$, the $8.8\,{\text{GHz}}$ clock for most of the digital circuitry, including PRBS generation, is obtained. An external PLL may be attached to stabilize the LO phase and reduce phase noise, if necessary.
The high symbol rate of $8.8\,{\text{GHz}}$ allows for the very fine range resolution
\begin{align}
	\Delta_r = \frac{c_0}{2f_{\text{s}}}\approx1.7\,{\text{cm}}.
\end{align}

The \textit{transmit}~(TX) side features a \textit{quadrature phase shift keying}~(QPSK) modulator that is employed for upconverting the PRBS with \textit{binary phase shift keying}~(BPSK) but also allows for a crude form of data transmission with $2$ bits per symbol. The modulator is followed by several buffers driving the four PAs that feed into the TX antennas.\\

On the \textit{receiver}~(RX) side, the signal from the RX antennas are combined and then amplified by a \textit{low-noise amplifier}~(LNA). This unusual choice is justified in Section~\ref{s:design:ant}. Following the LNA, two mixers are used to downconvert the RF signal with both $0^{\text{o}}$, or \textit{in-phase}~(I), and $90^{\text{o}}$, or \textit{quadrature}~(Q), to be able to detect incoming signals regardless of relative phase to the LO. Finally, several analog correlators with potentially differently shifted reference sequences can check for the presence of a target in the range bin corresponding to the respective time shift.\\

Tasks such as controlling the time shift for each correlator (locking on to a target and searching for targets) are handled by a micro-controller that communicates with the system using a primitive \textit{serial programming interface}~(SPI). In a finalized product one might internalize the control logic and handle communication through the $140\,{\text{GHz}}$ wireless channel.\\

While most other PMCW radar systems (externally) sample the \textit{intermediate frequency}~(IF) signal with an ADC and process it digitally (see \cite{pncwRadar} for an example), the present approach is much less complex to design and works for a large dynamic range without the need for a highly linear ADC with a high resolution -- the \textit{effective number of bits}~(ENOB) has approximately the same requirement as the number of bits of the LFSR determined in Section~\ref{ss:system:dyn:acfBase}.