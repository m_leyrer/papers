- To prepare a thesis: use template_thesis.tex

- To prepare a presentation: use template_presentation.tex

- To prepare a poster: use template_poster_landscape.tex or template_poster_portrait.tex

- For Announcements: use msvannounce.tex
