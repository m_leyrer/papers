% Encoding: UTF-8

@InProceedings{Graeb1993,
  author    = {H. E. Graeb and C. U. Wieser and K. J. Antreich},
  title     = {Improved Methods for Worst-Case Analysis and Optimization Incorporating Operating Tolerances},
  booktitle = {30th ACM/IEEE Design Automation Conference},
  year      = {1993},
  pages     = {142-147},
  month     = {June},
  doi       = {10.1145/157485.164641},
  issn      = {0738-100X},
  keywords  = {Circuit optimization;Circuit simulation;Circuit topology;Electronic design automation and methodology;Fluctuations;Integrated circuit synthesis;Manufacturing;Optimization methods;Performance analysis;Voltage},
}

@Article{Antreich1993,
  author  = {Antreich, Kurt J. And Graeb, Helmut E. And Wieser, Claudia U.},
  title   = {Practical Methods for Worst-Case and Yield Analysis of Analog Integrated Circuits},
  journal = {International Journal of High Speed Electronics and Systems},
  year    = {1993},
  volume  = {04},
  number  = {03},
  pages   = {261-282},
  doi     = {10.1142/S0129156493000121},
  eprint  = {http://www.worldscientific.com/doi/pdf/10.1142/S0129156493000121},
  url     = {http://www.worldscientific.com/doi/abs/10.1142/S0129156493000121},
}

@Article{Antreich1994,
  author   = {K. J. Antreich and H. E. Graeb and C. U. Wieser},
  title    = {Circuit analysis and optimization driven by worst-case distances},
  journal  = {IEEE Transactions on Computer-Aided Design of Integrated Circuits and Systems},
  year     = {1994},
  volume   = {13},
  number   = {1},
  pages    = {57-71},
  month    = {Jan},
  issn     = {0278-0070},
  doi      = {10.1109/43.273749},
  keywords = {CMOS integrated circuits;buffer circuits;circuit analysis computing;computational complexity;integrated logic circuits;switched capacitor filters;CMOS buffer amplifier;CMOS inverter;circuit analysis;circuit robustness;circuit simulators;computational complexity;design centering;deterministic method;integrated circuit design methodology;parametric circuit design;specification analysis;switched capacitor filter;tolerance bodies;worst-case distances;worst-case operating conditions;worst-case transistor model parameters;yield optimization;Circuit analysis;Circuit synthesis;Computational modeling;Design optimization;Integrated circuit manufacture;Integrated circuit measurements;Integrated circuit synthesis;Integrated circuit yield;Pulp manufacturing;Robustness},
}

@InProceedings{Rohde2015,
  author    = {J. Rohde and J. E. Stellet and H. Mielenz and J. M. Z\"ollner},
  title     = {Model-Based Derivation of Perception Accuracy Requirements for Vehicle Localization in Urban Environments},
  booktitle = {2015 IEEE 18th International Conference on Intelligent Transportation Systems},
  year      = {2015},
  pages     = {712-718},
  month     = {Sept},
  doi       = {10.1109/ITSC.2015.121},
  issn      = {2153-0009},
  keywords  = {Gaussian noise;estimation theory;probability;road vehicles;stereo image processing;traffic engineering computing;Gaussian noise;UAD;UDA;map-relative localization accuracy estimation;occluded lane markings;perception accuracy requirements;probabilistic model;stereo vision system;stochastic landmark distribution;urban automated driving;urban automated lane keeping system;urban driver assistance;vehicle localization;Accuracy;Monte Carlo methods;Noise;Noise measurement;Probabilistic logic;Robot sensing systems;Vehicles},
}

@InProceedings{Stellet2016,
  author    = {J. E. Stellet and P. Vogt and J. Schumacher and W. Branz and J. M. Z\"ollner},
  title     = {Analytical derivation of performance bounds of autonomous emergency brake systems},
  booktitle = {2016 IEEE Intelligent Vehicles Symposium (IV)},
  year      = {2016},
  pages     = {220-226},
  month     = {June},
  doi       = {10.1109/IVS.2016.7535389},
  keywords  = {braking;decision making;formal logic;state estimation;traffic engineering computing;AEB systems;Cramér-Rao bound;analytical derivation;autonomous emergency brake systems;decision making logic;noisy sensor measurements;objective metrics;performance bounds;state estimation covariance;uncertain prediction models;Acceleration;Accidents;Brakes;Measurement;Trajectory;Uncertainty;Vehicles},
}

@InProceedings{Stellet2015,
  author    = {J. E. Stellet and J. Schumacher and W. Branz and J. M. Z\"ollner},
  title     = {Uncertainty propagation in criticality measures for driver assistance},
  booktitle = {2015 IEEE Intelligent Vehicles Symposium (IV)},
  year      = {2015},
  pages     = {1187-1194},
  month     = {June},
  doi       = {10.1109/IVS.2015.7225844},
  issn      = {1931-0587},
  keywords  = {Monte Carlo methods;automobiles;collision avoidance;decision making;error statistics;intelligent transportation systems;motion control;nonlinear control systems;road safety;statistical distributions;uncertain systems;Monte-Carlo simulation;active safety system;automotive collision avoidance system;closed-form expression;collision probability;criticality measure;decision making;driver assistance system;error propagation;model-based criticality metrics;nonlinear motion model;probability distribution;sensor measurement;uncertainty propagation;Acceleration;Measurement uncertainty;Noise;Predictive models;Uncertainty;Vehicles},
}

@InProceedings{Zheng2003,
  author    = {Pengjun Zheng and M. McDonald},
  title     = {The effect of sensor errors on the performance of collision warning systems},
  booktitle = {Proceedings of the 2003 IEEE International Conference on Intelligent Transportation Systems},
  year      = {2003},
  volume    = {1},
  pages     = {469-474 vol.1},
  month     = {Oct},
  doi       = {10.1109/ITSC.2003.1251998},
  keywords  = {alarm systems;collision avoidance;measurement errors;optical radar;road traffic;sensors;velocity measurement;Honda algorithm;Mazda algorithm;NHTSA algorithm;collision warning algorithms;collision warning systems;direct measurements error;laser speedometer;lidar;national highway traffic safety administration;radar;sensor configurations;sensor errors;Alarm systems;Collision avoidance;Laser radar;Measurement errors;Road accidents;Sensor phenomena and characterization;Sensor systems;US Department of Transportation;Vehicles;Velocity measurement},
}

@Article{McKinsey2016,
  author = {{McKinsey \& Company}},
  title  = {Automotive revolution - perspective towards 2030},
  year   = {2016},
  month  = jan,
}

@InProceedings{Dirndorfer2011,
  author    = {Dirndorfer, Tobias and Botsch, Michael and Knoll, Alois},
  title     = {Model-Based Analysis of Sensor-Noise in Predictive Passive Safety Algorithms},
  booktitle = {Proceedings of the 22nd Enhanced Safety of Vehicles Conference},
  year      = {2011},
  abstract  = {The introduction of environment perception sensors into the automotive world enables further improvement of the already highly optimized passive safety systems. Such sensors facilitate the development of safety applications that can act in a context sensitive manner concerning the protection of vehicle occupants. Hereby the quality of the provided information is decisive for the usability and effective range of such sensors within integrated safety systems. In this paper noise effects in sensors and their implications on the prediction of collision parameters are analyzed. The focus lies on sensors that can measure distances but not velocities or accelerations of the objects surrounding the car. For such sensors a noise model is presented as well as a tracking algorithm aiming to estimate the velocities and to compensate the effects of noise. This information is used by a trajectory-based algorithm to predict relevant collision parameters like time-to-collision, relative velocity at collision time etc. Monte Carlo simulations show the influence of noise on the accuracy of the predicted collision parameters. The described model-based study allows the systematic deduction of sensor requirements and represents a new way for the evaluation of the robustness of predictive passive safety systems.},
}

@InProceedings{Nilsson2010,
  author    = {J. Nilsson and M. Ali},
  title     = {Sensitivity analysis and tuning for active safety systems},
  booktitle = {13th International IEEE Conference on Intelligent Transportation Systems},
  year      = {2010},
  pages     = {161-167},
  month     = {Sept},
  doi       = {10.1109/ITSC.2010.5625103},
  issn      = {2153-0009},
  keywords  = {driver information systems;road safety;vehicles;active safety systems;driver assistance;sensitivity analysis;traffic situation;tuning;vehicle state;Driver circuits;Safety;Sensor systems;System performance;Upper bound;Vehicles;Active Safety;Automotive;Decision Making;Performance Evaluation;Sensitivity Analysis;Tuning},
}

@MastersThesis{Utschick1993,
  author = {W. Utschick},
  title  = {{E}in sequentiell quadratisches {O}ptimierverfahren zur {T}oleranzanalyse integrierter {S}chaltungen},
  school = {TUM},
  year   = {1993},
  type   = {Diplomarbeit},
}

@Comment{jabref-meta: databaseType:bibtex;}
