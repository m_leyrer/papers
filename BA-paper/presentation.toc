\babel@toc {english}{}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{System Model}{4}{0}{2}
\beamer@sectionintoc {3}{WCD Approach}{8}{0}{3}
\beamer@sectionintoc {4}{OWCD Approach}{10}{0}{4}
\beamer@sectionintoc {5}{Numerical Examples}{13}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{15}{0}{6}
