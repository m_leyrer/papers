\documentclass[10pt,t]{beamer}
\usepackage{msvcommon}

% -------------------------------------------------------------------------------
% ** Input Encoding
% - utf8x is recommended at MSV
% - Some comments on encoding can be found here:
%   https://groups.google.com/forum/?fromgroups=#!msg/comp.text.tex/4LC-xODb-LU/1Bd5UZOMNM4J
%
\ifxetexorluatex
  % XeTeX and LuaTeX support utf8 by default
\else
  % File encoding utf8x
  \usepackage{ucs}
  \usepackage[utf8x]{inputenc}
\fi
%
%--------------------------------------------------------------------------------
% ** Language Settings and Hyphenation Patterns
%
% Note: final argument to babel sets the main language
%
\ifxetex%
  \usepackage{polyglossia}
  \setmainlanguage{english}
  %\setotherlanguage{german}
%  \setmainlanguage{german}
%  \setotherlanguage{english}
\else
  \usepackage[english]{babel} 
\fi


\usepackage{bibentry}

%--------------------------------------------------------------------------------
% ** MSV style defintions for beamer presentations
%
\usepackage[colorblock]{msvpresentation}
%-------------------------------------------------------------------------------
% ** Options for msvpresentation (all options are disabled by default)
% 1) boolean options for msvpresentation package:
%  | bw             | black and white color scheme                             |
%  | colorblock     | use non-white background colors for blocks               |
%  | frameblock     | use colored borders for blocks                           |
%
%--------------------------------------------------------------------------------
% ** Setup look of presentation
%
% MSV style should be compatible with the beamer themes
%
\useinnertheme{default}
\useoutertheme{default}

% Colors:
%\usecolortheme[named=TUMDarkerBlue]{structure} % variant: TUMBlack
\usecolortheme[named=TUMBlack]{structure} % variant: TUMBlack
\setbeamercolor{frametitle}{fg=TUMBlack} % variant: TUMDarkerBlue
\setbeamercolor{normal text}{fg=TUMBlack,bg=TUMWhite} % font/background
\setbeamercolor{alerted text}{fg=TUMBeamerRed,bg=TUMWhite} % alert boxes
\setbeamercolor{example text}{fg=TUMBeamerGreen,bg=TUMWhite} % example boxes

% Headers, Footers, Titlepage:
%   Note: can adjust height of the frame title with \raisetitle and \lowertitle
\setbeamertemplate{headline}[tum] % no, tum, msvtum
\setbeamertemplate{footline}[title] % minimal, author, authortitle
\setbeamertemplate{title page}[tum][left] % with clock tower
%\setbeamertemplate{title page}[default][left,sep=0pt] % without clock tower

%--------------------------------------------------------------------------------
% ** Presentation title, author, etc.
%
\title{An Efficient Approach to Simulation-Based Robust Function and Sensor Design Applied to an Automatic Emergency Braking System}
\author[Michael Leyrer]{Michael L. Leyrer$^1$, Christoph St\"ockle$^{1,2}$, Stephan Herrmann$^{2}$, Tobias Dirndorfer$^{2}$, Wolfgang Utschick$^{1}$}
\institute{$^1$Methods of Signal Processing, Technische Universität München, 80290 Munich, Germany\\$^2$AUDI AG, 85045 Ingolstadt, Germany}
\date{\today}

\begin{document}
{ % use custom header/footer for title page
\setbeamertemplate{headline}[tum]
\setbeamertemplate{footline}[no]
\begin{frame}
\titlepage
\end{frame}
}

\section{Motivation}

\begin{frame}
	\frametitle{Motivation (1)}
	
	%	Car manufacturers implement vehicular safety functions which improve passenger safety. As those rely on sensor measurements to interpret the driving situation, random sensor measurement errors have to be taken into account when designing a safety system.
	\vspace{.5cm}
	Vehicular safety systems improve passenger safety but are subject to measurement errors
	
	\vspace{1cm}
	
	\begin{center}
		\begin{tikzpicture}
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,blue] (sensors)  at (1.5,0) {sensors};
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,red] (function) at (5.5,0) {function};
		\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,green] (actor) at (8.5,0) {action};
		
		\draw[-latex] (0,0) node[left]{situation} -- (sensors);
		\draw[-latex](1.5,-0.75) node[below]{errors} -- (sensors);
		\draw[-latex](sensors) -- node[midway,above]{measurement} (function);
		\draw[-latex](function) -- node[above,midway]{decision} (actor);
		\end{tikzpicture}
	\end{center}
	\vspace{.7cm}
	%	Choosing arbitrarily good sensor is not a good design solution because very good sensor cost a lot. Therefore the task in designing a robust system is to minimize cost while still achieving a minimal quality measure when the function parameters are optimal. This can be formulated as \cite{StoeckleAEB},\cite{StoeckleAEB2}
	
	Robust design is needed:
	
	\begin{itemize}
		\item System designed such that it meets certain requirements in a robust manner despite sensor measurement errors
		\item Can be formulated as an optimization problem using a quality measure
	\end{itemize}
	%	\begin{equation}\label{eq:task}
	%		(\boldsymbol{\tau}_{\text{opt}},\boldsymbol{\sigma}_{\text{opt}})=\underset{\boldsymbol{\tau},\boldsymbol{\sigma}}{\arg\min}\:c(\boldsymbol{\sigma})\quad \text{s.t.}\quad Q(\boldsymbol{\tau},\boldsymbol{\sigma})\geq Q_{\text{min}}
	%	\end{equation}
\end{frame}

\begin{frame}
	\frametitle{Motivation (2)}
	
	\vspace{1cm}
	
	The quality measure is often a probability which has to be evaluated by simulating the system
	
	\vspace{1cm}
	Methods like Monte Carlo~(MC) simulation are computationally expensive
	
	\vspace{1cm}
	
	\begin{block}{Task}
		Find a computationally less expensive method for evaluating probabilities in the context of vehicular safety systems.
	\end{block}
	%	The quality measure chosen is defined as the probability of fulfilling a certain performance specification. In order to solve \eqref{eq:task} one has to calculate this probability, which can often only be done by simulating the system at hand.\\
	%	
	%	\bigskip
	%	
	%	The naive approach would be a Monte Carlo~(MC) simulation of the system, but this method involving many simulation runs with different realizations of the random measurement errors is very computationally expensive.
	%	
	%	\bigskip
	%	
	%	\begin{block}{}
	%		Finding a less computationally expensive method can significantly speed up the design process of vehicular safety systems.
	%	\end{block}
	
\end{frame}

\section{System Model}

\begin{frame}
	\frametitle{System Model\footnote{C. Stöckle, W. Utschick, S. Herrmann, and T. Dirndorfer, “Robust
			design of an automatic emergency braking system considering sensor
			measurement errors,” 21st IEEE International Conference on Intelligent
			Transportation Systems (ITSC), pp. 2018–2023, Nov 2018.}}
	
	%	The test function used here is automatic emergency braking~(AEB). It triggers a braking maneuver if a dangerous situation is detected.
	%	
	Considered scenario for time $t$:
	
	\begin{center}
		\begin{tikzpicture}
		\draw[->] (-2.1,0) -- (6.5,0);
		%		\fill[violet!30] (1.45,0) rectangle (3.2,1);
		%		\draw[violet,<->,thick] (1.45,0) -- (3.2,0);
		\draw[dashed] (0,-0.1) node[below]{$x_\text{ego}\left(t\right)$} -- (0,1);
		\draw[dashed] (3.25,-0.1) node[below]{$x_\text{obj}\left(t\right)$} -- (3.25,1);
		\draw[->] (0,0.5) -- ++(0.3,0) node[right]{$v_\text{ego}\left(t\right)$};
		\draw[->] (5.25,0.5) -- ++(0.15,0) node[right]{$v_\text{obj}\left(t\right)$};
		\node[inner sep=0pt] (ego-vecicle) at (-1,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_blue.pdf}};
		\node[inner sep=0pt] (object) at (4.25,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_red.pdf}};
		\node[above] at (-1,1) {ego vehicle};
		\node[above] at (4.25,1) {object};
%		\node[align=center,violet] at (2.325,0.5) {acceptable\\interval};
		\end{tikzpicture}
	\end{center}
	\vspace{.4cm}
	
	Relative position and velocity are sampled with a fixed frequency $f_\text{s}$, i.e., $t_n=\frac{n}{f_\text{s}}$:
	
	\begin{equation}
	\begin{bmatrix}
	x[n]\\v[n]
	\end{bmatrix} = 
	\begin{bmatrix}
	x(t_n)\\v(t_n)
	\end{bmatrix} = 
	\begin{bmatrix}
	x_{\text{obj}}(t_n) - x_{\text{ego}}(t_n)\\v_{\text{obj}}(t_n) - v_{\text{ego}}(t_n)
	\end{bmatrix}
	\end{equation}
	
	\vspace{.6cm}
	Both are measured with errors which are assumed to be Gaussian: 
	\begin{equation}
	\begin{bmatrix}
	\hat{x}[n]\\\hat{v}[n]
	\end{bmatrix}=\begin{bmatrix}
	x[n]\\v[n]
	\end{bmatrix} + \begin{bmatrix}
	\varepsilon_{x}[n]\\\varepsilon_{v}[n]
	\end{bmatrix},\quad \varepsilon_{x}[n]\sim\mathcal{N}(0,\sigma_x^2),\: \varepsilon_{v}[n]\sim\mathcal{N}(0,\sigma_v^2)
	\end{equation}
	
\end{frame}

\begin{frame}
	\frametitle{System Model - Triggering Rules}
	\begin{itemize}
		\item Decision rule based on time-to-collision~(TTC) $-\frac{\hat{x}[n]}{\hat{v}[n]}$ with a threshold $\tau$:\\Trigger a braking maneuver in time step $n$ if
		
		\begin{equation}
		\hat{x}[n]\leq -\tau\hat{v}[n].
		\end{equation}
		
		\vspace{.2cm}
		
		$\Rightarrow$ linear decision boundary in the $\hat{x}[n]$-$\hat{v}[n]$-space.
		
		\bigskip
		
		\item Decision rule based on brake-threat-number~(BTN) $\frac{\hat{v}^2[n]}{2\hat{x}[n]}$ with a threshold $\tau$:\\Trigger a braking maneuver if
		
		\begin{equation}
		\hat{x}[n]\leq \frac{\hat{v}[n]^2}{2\tau}
		\end{equation}
		
		\vspace{.2cm}
		
		$\Rightarrow$ non-linear decision boundary in the $\hat{x}[n]$-$\hat{v}[n]$-space.
	\end{itemize}
\end{frame}

%\begin{frame}
%	\frametitle{System Model - Simulation Parameters}
%	\vspace{.3cm}
%	The following parameters were used in system simulations:
%	
%	\vspace{.5cm}
%	
%	
%
%	\vspace{.5cm}
%	
%	$f_\text{s}$ is kept variable due to its influence on computational effort
%	
%\end{frame}

\begin{frame}
	\frametitle{Robust Design\footnote{C. Stöckle, S. Herrmann, T. Dirndorfer, and W. Utschick, “Robust function
			and sensor design for automated vehicular safety systems,” IEEE
			Signal Processing Magazine, Special Issue on Autonomous Driving, July
			2020.}}
	\begin{equation}
		(\tau_\text{opt},\sigma_{x,\text{opt}},\sigma_{v,\text{opt}})=\arg\min_{\tau,\sigma_x,\sigma_v}\:c\quad\text{s.t.}\quad Q(\tau,\sigma_x,\sigma_v)\geq Q_\text{min}
	\end{equation}
	
		\vspace{.2cm}
	
	$c$ : cost function, e.g., $c=-(\sigma_x + \sigma_v)$
	
	\vspace{.2cm}
	
	$Q_\text{min}$ : required quality level
	
	\vspace{.2cm}
	
	$Q$ : probability $\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max})$ that final distance between ego vehicle and object after the braking maneuver is in $[x_\text{min},x_\text{max}]$
	
	\begin{center}
		\begin{tikzpicture}
		\draw[->] (-2.1,0) -- (6.5,0);
				\fill[violet!30] (1.45,0) rectangle (3.2,1);
				\draw[violet,<->,thick] (1.45,0) -- (3.2,0);
		\draw[dashed] (0,-0.1) node[below]{$x_\text{ego}\left(t\right)$} -- (0,1);
		\draw[dashed] (3.25,-0.1) node[below]{$x_\text{obj}\left(t\right)$} -- (3.25,1);
		\draw[->] (0,0.5) -- ++(0.3,0) node[right]{$v_\text{ego}\left(t\right)$};
		\draw[->] (5.25,0.5) -- ++(0.15,0) node[right]{$v_\text{obj}\left(t\right)$};
		\node[inner sep=0pt] (ego-vecicle) at (-1,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_blue.pdf}};
		\node[inner sep=0pt] (object) at (4.25,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_red.pdf}};
		\node[above] at (-1,1) {ego vehicle};
		\node[above] at (4.25,1) {object};
		\node[align=center,violet] at (2.325,0.5) {acceptable\\interval};
		\end{tikzpicture}
	\end{center}

	
	
	In general, $Q$ can only be evaluated by system simulations
\end{frame}

\begin{frame}
	\frametitle{Monte Carlo (MC) Simulation}
	
	
	\vspace{.5cm}
	
	Approximating a probability $\mathsf{P}(f(\mathsf{s})\in \mathcal{A})$ with random variable $\mathsf{s}$, function $f$ and acceptance interval $\mathcal{A}$: 
	
	\bigskip
	
	\begin{enumerate}\setlength\itemsep{.4cm}
		\item Generate $n$ realizations of $\mathsf{s}$
		\item Evaluate $f(s_i)$ for all realizations $s_i$
		\item Count how many $f(s_i)\in \mathcal{A}$ $\Rightarrow$ $n_\mathcal{A}$
		\item $\mathsf{P}(f(\mathsf{s})\in \mathcal{A})\approx \frac{n_\mathcal{A}}{n}$
	\end{enumerate}

	\vspace{.8cm}
	
	$\Rightarrow$ Very generic but high accuracy requires many samples! (when not optimized)
	
\end{frame}

\section{WCD Approach}

\begin{frame}
	\frametitle{Worst-Case-Distance (WCD) Approach\footnote{H. E. Graeb, C. U. Wieser, and K. J. Antreich, ''Improved methods for worst-case analysis and optimization incorporating operating tolerances,'' 30th ACM/IEEE Design Automation Conference, pp. 142–147, June 1993.}}
	$\boldsymbol{s}=[s_1,...,s_N]^{\text{T}}\sim \mathcal{N}(\boldsymbol{s}_0,\boldsymbol{C})$,\\
	\vspace{.3cm}acceptance region $\mathcal{A}$,\\
	
	$\rightarrow$ approximate probability\\
	$\mathsf{P}(\boldsymbol{s}\in \mathcal{A})\approx\begin{cases}
	\Phi(\beta_\text{w}),&{\boldsymbol{s}_0}\in{\mathcal{A}}\\\Phi(-\beta_\text{w}),&{\boldsymbol{s}_0}\in\overline{\mathcal{A}}
	\end{cases}$\\
%	\vspace{.3cm}linear approximation:\\$
%%	\begin{equation}
%		\mathcal{A}_{\boldsymbol{s}} \approx \{\boldsymbol{s}\in \mathbb{R}^n|\boldsymbol{a}^{\text{T}}\boldsymbol{C}^{-1}\boldsymbol{s}\leq \beta_\text{w}\},$
%	\end{equation}
		
	\vspace{.5cm}
	where\\
%	where $||\boldsymbol{a}||_\beta=1$,\\\vspace{.3cm}
%	\begin{equation}
		$\beta_\text{w}^2=\min_{\boldsymbol{s}\in \mathbb{R}^N}\beta^2(\boldsymbol{s})\quad\text{s.t.}\quad \boldsymbol{s}\in \partial\mathcal{A},$\\\vspace{.3cm}
%	\end{equation}
	and\\ $\beta^2({\boldsymbol{s}})=(\boldsymbol{s}-{\boldsymbol{s}_0})^{\text{T}}\boldsymbol{C}^{-1}(\boldsymbol{s}-{\boldsymbol{s}_0})$
	
	\vspace{.6cm}
	
	
%	\begin{equation}
		
%	\end{equation}
	

	\vspace{-5cm}
	
	{
	\hspace{5.5cm}
	\begin{tikzpicture}[scale=.6]
	\draw[->] (-4.5,0)--(4.5,0) node[anchor=west]{$s_1$};
	\draw[->] (0,-4.5)--(0,4.5) node[anchor=west]{$s_2$};
	%		\foreach \x in {-4,...,4}{
	%			\draw (\x,-.1)--(\x,.1)(-.1,\x)--(.1,\x);
	%		}
	\draw[blue, very thick, dashed] (-2,4.5)--(4.5,-.35) (3.5,-3.5)node[]{$\mathcal{A}$}(3.5,3.5)node[]{$\overline{\mathcal{A}}$};
	\draw[blue,very thick](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)node[anchor=west]{$\partial {\mathcal{A}}$};
	\fill[blue, opacity=.1](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)|-(-4.5,-4.5)|-cycle;
	%\fill[blue, opacity=.1] (-2,4.5)--(4.5,-.35)|-(-4.5,-4.5)|-cycle;
	\foreach \x in {1,2,3}
	\draw[red, thin,dashed,font=\tiny] (0,0) circle[x radius=1.5*\x, y radius=\x] (0,-\x) node[anchor=north east]{$\beta = \x$};
	\draw[red,thick,|-|](0,0)--(2.26,1.33);
	\draw[red](1.4,.5)node[rotate=30]{$\beta_\text{w}=2$};
	\end{tikzpicture}
	
	}
	
\end{frame}


\begin{frame}
	\frametitle{WCD Approach Directly Applied to Robust Design}
	Measurement errors form the vector $\boldsymbol{s}$\\
	
	\vspace{.3cm}
	Two WCDs $\beta_{\text{w,min}}$ and $\beta_{\text{w,max}}$ corresponding to acceptance regions $x_\text{end}\geq x_\text{min}$ and $x_\text{end}\leq x_\text{max}$ respectively\\
	\vspace{-.2cm}
	\begin{equation*}
		{\boldsymbol{\Downarrow}}
	\end{equation*}
	\vspace{-.7cm}
	\begin{align}\nonumber
		\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max})=\mathsf{P}(x_\text{end}\leq x_\text{max})-\mathsf{P}(x_\text{end}\leq x_\text{min})\\
		\approx \Phi(\beta_{\text{w,max}})-\Phi(-\beta_{\text{w,min}})=\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})
	\end{align}
	
	\vspace{.5cm}
	
	under the assumption that $x_\text{min}\leq x_\text{end}\leq x_\text{max}$ for $\boldsymbol{s}=\boldsymbol{0}$
	
	\vspace{.2cm}
	
	\begin{alertblock}
		{However,} $\beta_{\text{w,min}}$ and $\beta_{\text{w,max}}$ are difficult to obatain and approximation becomes inaccurate for larger standard deviations of the error.
	\end{alertblock}
	
\end{frame}
%
%\begin{frame}
%	$Q$ vs. $\sigma_x$ and $\tau$ for TTC-based decision rule, $\sigma_v=0$ and $f_\text{s}=100\text{Hz}$
%	
%			\vspace{.5cm}
%			\includegraphics[width=25cm]{plots/actual2d100Hz.pdf}\\
%			
%			\vspace{-13.7cm}
%			
%			{\hspace*{6cm}\includegraphics[width=25cm]{plots/wca2d100Hz.pdf}}
%		
%		\vspace{-10cm}
%		
%		\begin{center}
%			\begin{tabular}{ c | c }
%				$x_0$&$10\text{m}$\\\hline
%				$v_0$&$-10\frac{\text{m}}{\text{s}}$\\\hline
%				$x_\text{min}$&$0\text{m}$\\\hline
%				$x_\text{max}$&$0.5\text{m}$\\\hline
%				$a_\text{b}$&$10\frac{\text{m}}{\text{s}^2}$
%			\end{tabular}
%		\end{center}
%\end{frame}
%
%\begin{frame}
%	Approximation error vs. $\sigma_x$ and $\tau$\\
%	\vspace{-.3cm}
%	{\hspace*{1.3cm}\includegraphics[width=40cm]{plots/wcaVsActual_error2d100Hz.pdf}}\\\vspace{-15.7cm}
%	$\Rightarrow$ straightforward application of WCD approach not well-suited
%\end{frame}

%\begin{frame}
%	\frametitle{Illustration of the WCD Approach}
%	\begin{center}
%		\begin{tikzpicture}[scale=.8]
%		\draw[->] (-4.5,0)--(4.5,0) node[anchor=west]{$s_1$};
%		\draw[->] (0,-4.5)--(0,4.5) node[anchor=west]{$s_2$};
%		%		\foreach \x in {-4,...,4}{
%		%			\draw (\x,-.1)--(\x,.1)(-.1,\x)--(.1,\x);
%		%		}
%		\draw[blue, very thick, dashed, font=\Large] (-2,4.5)--(4.5,-.35) (3.5,-3.5)node[]{$\mathcal{A}_{\boldsymbol{s}}$};
%		\draw[blue,very thick](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5);
%		\fill[blue, opacity=.1](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)|-(-4.5,-4.5)|-cycle;
%		%\fill[blue, opacity=.1] (-2,4.5)--(4.5,-.35)|-(-4.5,-4.5)|-cycle;
%		\foreach \x in {1,2,3}
%		\draw[red, thick] (0,0) circle[x radius=1.5*\x, y radius=\x] (0,-\x) node[anchor=north east]{$\beta = \x$};
%		\draw[very thick, ->] (1,.3) node[]{$\boldsymbol{a}$} (0,0)--(2.26/2,1.33/2);
%		\draw[red,thick,|-|](0,0)--(2.26,1.33);
%		\draw[red](1.9,.8)node[rotate=30]{$\beta_\text{w}=2$};
%		\end{tikzpicture}
%	\end{center}
%\end{frame}


\section{OWCD Approach}

%\begin{frame}
%	\frametitle{Structure of the Acceptance Region}
%	TTC-based triggering, $\sigma_v=0$
%	\vspace{.5cm}
%	
%	Boundary acceptance region $\mathcal{A}_{\boldsymbol{s}}$ consists of many orthogonal hyperplanes:
%	
%	$\mathcal{B}_i$ : set of errors, for which no intervention is triggered in time step $i$
%	
%	\vspace{.2cm}
%	\begin{center}
%		\begin{tabular}{c c}
%		\begin{tikzpicture}[scale=0.6]
%		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,3}$};
%		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,4}$};
%		
%		\draw[blue, dashed] (-3.3,-.4)-|(-1,-3.3);
%		\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
%		\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
%		\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
%		\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
%		\draw[blue, font=\large] (-2.5,1.7)node[]{$\mathcal{B}_{4}$} (1.5,-2.5)node[]{$\mathcal{B}_{3}$} (1.7,1.7)node[]{$\mathcal{B}_{3}\cap \mathcal{B}_{4}\supseteq \mathcal{A}_{\boldsymbol{s}}$};
%		\draw[red, thick,|-|] (1,-.4)--(1,0)node[anchor= south]{$\beta_{\text{w,max}}$};
%		\end{tikzpicture}  &
%		\begin{tikzpicture}[scale=0.6]
%		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,5}$};
%		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,6}$};
%		\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
%		\draw[blue, dashed] (.2,-3.3)|-(-3.3,.8);
%		\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
%		\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
%		\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
%		\draw[blue, font=\large] (-2,2.2)node[]{$\mathcal{B}_{6}$} (1.7,-2) node[]{$\mathcal{B}_5$} (-2.2,-1.7)node[]{$(\mathcal{B}_5\cap \mathcal{B}_6)^\text{C} \supseteq \mathcal{A}_{\boldsymbol{s}}$};
%		\draw[red, thick,|-|] (.2,.8)--(0,0)node[anchor= south east]{$\beta_{\text{w,min}}$};
%		\draw[blue,thick](1.7,1.7)node[]{$\mathcal{B}_{5}\cap \mathcal{B}_{6}$};
%		\end{tikzpicture}\\
%		must not trigger&must trigger at least once
%	\end{tabular}
%	\end{center}
%\end{frame}

%\begin{frame}
%	\frametitle{OWCD Approach - Boundary Visualization (1)}
%	\begin{center}
%		\begin{tikzpicture}
%		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,3}$};
%		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,4}$};
%		
%		\draw[blue, dashed] (-3.3,-.4)-|(-1,-3.3);
%		\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
%		\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
%		\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
%		\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
%		\draw[blue, font=\large] (-2.5,1.7)node[]{$\mathcal{B}_{4}$} (1.5,-2.5)node[]{$\mathcal{B}_{3}$} (1.7,1.7)node[]{$\mathcal{B}_{3}\cap \mathcal{B}_{4}\supseteq \mathcal{A}_{\boldsymbol{s}}$};
%		\draw[red, thick,|-|] (1,-.4)--(1,0)node[anchor= south]{$\beta_{\text{w,max}}$};
%		\end{tikzpicture}
%	\end{center}
%\end{frame}
%
%\begin{frame}
%	\frametitle{OWCD Approach - Boundary Visualization (2)}
%	\begin{center}
%		\begin{tikzpicture}
%		\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x,5}$};
%		\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x,6}$};
%		\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
%		\draw[blue, dashed] (.2,-3.3)|-(-3.3,.8);
%		\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
%		\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
%		\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
%		\draw[blue, font=\large] (-2,2.2)node[]{$\mathcal{B}_{6}$} (1.7,-2) node[]{$\mathcal{B}_5$} (-1.7,-1.7)node[]{$(\mathcal{B}_5\cap \mathcal{B}_6)^\text{C} \supseteq \mathcal{A}_{\boldsymbol{s}}$};
%		\draw[red, thick,|-|] (.2,.8)--(0,0)node[anchor= south east]{$\beta_{\text{w,min}}$};
%		\draw[blue,thick](1.7,1.7)node[]{$\mathcal{B}_{5}\cap \mathcal{B}_{6}$};
%		\end{tikzpicture}
%	\end{center}
%\end{frame}
\begin{frame}
	\frametitle{Orthogonal WCD (OWCD) Approach}
	\vspace*{1cm}
	\begin{block}
		{Basic Idea}\large
		Apply the WCD approach multiple times for mutually stochastically independent sets of random variables and combine results properly. This allows for simplifications when determining the WCDs.
	\end{block}
\end{frame}


\begin{frame}
	\frametitle{Orthogonal WCD (OWCD) Approach Applied (1)}
	
%	\vspace{.5cm}
	
	\begin{block}
		{In Our Case}
		Random variables in different time steps are independent (the $\partial{\mathcal{T}_n}$ are orthogonal) $\rightarrow$ one WCD for each time step
	\end{block}

	\vspace{-.2cm}
	
	\begin{center}
		\begin{tabular}{c c}
			\begin{tikzpicture}[scale=0.6, transform shape]
			\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x}[3]$};
			\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x}[4]$};
			
			\draw[blue, dashed] (-3.3,-.4)node[anchor=east]{$\partial \mathcal{T}_4$}-|(-1,-3.3)node[anchor=north]{$\partial \mathcal{T}_3$};
			\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
			\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
			\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
			\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
			\draw[blue, font=\large] (-2.5,1.7)node[]{$\overline{\mathcal{T}}_{4}$} (1.5,-2.5)node[]{$\overline{\mathcal{T}}_3$} (1.7,1.7)node[]{$\overline{\mathcal{T}}_3\cap \overline{\mathcal{T}}_{4}\supseteq \mathcal{A}$};
			%				\draw[red, thick,|-|] (1,-.4)--(1,0)node[anchor= south]{$\beta_{\text{w,max}}$};
			\draw[black,thick](-1,-.1)-|(-.7,-.4);
			\draw[thick,red,<->](-1,1)--(0,1)node[midway,anchor=south]{$\beta_{\text{w},3}$};
			\draw[thick,red,<->](1,0)--(1,-.4)node[midway,anchor=west]{$\beta_{\text{w},4}$};
			%				\draw[black, thick](-1,-.1)..controls(-.8,-.1)and(-.7,-.2)..(-.7,-.4)(-.88,-.3)node[]{$\boldsymbol{\cdot}$};
			\end{tikzpicture}  &
			\begin{tikzpicture}[scale=0.6, transform shape]
			\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x}[5]$};
			\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x}[6]$};
			\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
			\draw[blue, dashed] (.2,-3.3)node[anchor=north]{$\:\,\partial \mathcal{T}_5$}|-(-3.3,.8)node[anchor=east]{$\partial \mathcal{T}_6$};
			\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
			\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
			\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
			\draw[blue, font=\large] (-2,2.2)node[]{$\overline{\mathcal{T}}_{6}$} (1.7,-2) node[]{$\overline{\mathcal{T}}_5$} (-1.7,-1.7)node[]{$\overline{(\overline{\mathcal{T}}_5\cap \overline{\mathcal{T}}_6)} \supseteq \mathcal{A}$};
			%				\draw[red, thick,|-|] (.2,.8)--(0,0)node[anchor= south east]{$\beta_{\text{w,min}}$};
			\draw[blue, font=\large](1.7,1.7)node[]{$\overline{\mathcal{T}}_{5}\cap \overline{\mathcal{T}}_{6}$};
			\draw[black,thick](.2,1.1)-|(.5,.8);
			\draw[thick,red,<->](.2,1.2)--(0,1.2)node[anchor=east]{$\beta_{\text{w},5}$};
			\draw[thick,red,<->](1,0)--(1,.8)node[midway,anchor=west]{$\beta_{\text{w},6}$};
			%				\draw[black, thick](.2,1.1)..controls(.4,1.1)and(.5,1)..(.5,.8)(.32,.9)node[]{$\boldsymbol{\cdot}$};
			\end{tikzpicture}
		\end{tabular}
	\end{center}
	

		\vspace{-.2cm}
%	\begin{equation}
%		\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max}) = \left(\prod_{i\in \mathcal{I}_{\text{bad}}}\mathsf{P}(\overline{T}_i)\right)\left(1-\prod_{i\in \mathcal{I}_{\text{good}}}\mathsf{P}(\overline{T}_i)\right)
%	\end{equation}
	
	\vspace{-.2cm}
	
	\begin{align}
		\beta_{\text{w},n}^2 = \min_{{\boldsymbol{\varepsilon}[n]}\in\mathbb{R}^2}\beta^2({\boldsymbol{\varepsilon}[n]}),\quad{\text{s.t.}}\quad{\boldsymbol{\varepsilon}[n]}\in \partial {\mathcal{T}}_n\\
		{\boldsymbol{\varepsilon}}\in{\mathcal{T}_n}\iff {\text{intervention triggered in time step } n}\nonumber
	\end{align}
	
	
	
\end{frame}

\begin{frame}
	\frametitle{Orthogonal WCD (OWCD) Approach Applied (2)}
	Probability of not triggering an intervention in time step $n$
	\begin{equation}\label{eq:owcdProb}
	\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)\approx\begin{cases}
	\Phi(\beta_{\text{w},n}),&\quad{\boldsymbol{0}}\in \overline{\mathcal{T}}_n\\
	\Phi(-\beta_{\text{w},n}),&\quad{\boldsymbol{0}}\in\mathcal{T}_n
	\end{cases}
	\end{equation}
	\begin{equation*}
		{\boldsymbol{\Downarrow}}
	\end{equation*}
	\vspace{-.9cm}
	\begin{align}
	\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})=\mathsf{P}\left({\boldsymbol{\varepsilon}}\in\underset{n\in{\mathcal{I}_A}}{\bigcap}\overline{\mathcal{T}}_n \cap \underset{n\in{\mathcal{I}_B}}{\bigcup}\mathcal{T}_n\right)\nonumber\\
	=\left(\prod_{n\in{\mathcal{I}_A}}\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)\right)\left(1-\prod_{n\in{\mathcal{I}_B}}\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)\right)\label{eq:owcdProbProd}
	\end{align}
	
	\vspace{.2cm}
	$\mathcal{I}_A$: set of time steps in which an intervention must not be triggered ("too early")\\
%	\vspace*{.2cm}
	$\mathcal{I}_B$: set of time steps in which an intervention must be triggered (at least once)\\
	\vspace{.3cm}
	Index sets can be determined by a few simulations of the system and are independent of statistical parameters $\rightarrow$ only required once
\end{frame}

%\begin{frame}
%	\frametitle{OWCD Approach - Results for TTC-Based Decision Rule}
%	$\sigma_v=0$ and $f_\text{s}=100\text{Hz}$
%	\vspace{.1cm}
%	
%	Decision boundary is linear $\Rightarrow$ OWCD approach gives exact results!
%	
%	\vspace{-.2cm}
%		
%			\includegraphics[width=25cm]{plots/actual2d100Hz.pdf}\\
%	
%	
%			\vspace{-13.67cm}\hspace*{6cm}\includegraphics[width=25cm]{plots/owca2d100Hz.pdf}
%	\vspace{-10cm}
%	
%	Example: $\tau=0.521\text{s}$, $\sigma_x=0.143\text{m}$ yields $\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})=0.992$
%	\begin{itemize}
%		\item Determining index sets requires $100$ system simulations
%		\item Evaluating WCDs requires $4508$ evaluations of decision rule
%		\item MC: $5\cdot10^6$ simulations for confidence interval $[0.992-10^{-4},0.992+10^{-4}]$ at confidence level $99\%$\footnote[2]{H. E. Graeb, Analog Design Centering and Sizing. Springer Netherlands,
%			2007.}
%	\end{itemize}
%	
%
%\end{frame}
%
%\begin{frame}
%	\frametitle{OWCD Approach - Results for BTN-Based Decision Rule}
%	$f_\text{s}=50\text{Hz}$
%	
%	\vspace{.1cm}
%	
%	Despite non-linear boundary OWCD approach still gives good results
%	
%	\vspace{.1cm}
%	
%%	\begin{itemize}\setlength\itemsep{.3cm}
%%		\item $1263$ function and constraint evaluations using \texttt{fmincon()} 
%%		\item Error at $\mathsf{P}=0.9972$: $-3.5\cdot10^{-5}$
%%		\item Monte Carlo would need $2\cdot10^{6}$\\evaluations for similar results
%%	\end{itemize}
%	
%	
%	\vspace{-.0cm}\hspace*{0cm}\includegraphics[width=5cm]{plots/omc1e6BTN3d50Hz.pdf}
%	
%	\vspace{-2.5cm}\hspace*{5.5cm}\includegraphics[width=5cm]{plots/owcaBTN3d50Hz.pdf}
%	
%	{\tiny\hspace{.7cm}{Contour surfaces of $\mathsf{P}(x_\text{min}\leq x_\text{end}\leq x_\text{max})$}\hspace{1.8cm}{Contour surfaces of $\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})$}}
%	
%	
%	\vspace{.15cm}
%	Example: $\tau=9.31\frac{\text{m}}{\text{s}^2}$, $\sigma_x=0.0724\text{m}$, $\sigma_v=0.0724\frac{\text{m}}{\text{s}}$ yields $\hat{\mathsf{P}}(x_\text{min}\leq x_\text{end}\leq x_\text{max})=0.99722$ and error $\hat{\mathsf{P}}-\mathsf{P}=-3.5\cdot 10^{-5}$
%	\begin{itemize}
%		\item Determining index sets requires $50$ system simulations
%		\item Evaluating WCDs requires $1263$ evaluations of decision rule
%		\item MC: $2\cdot10^6$ simulations for confidence interval $[0.99725-10^{-4},0.99725+10^{-4}]$ at confidence level $99\%$\footnote[3]{H. E. Graeb, Analog Design Centering and Sizing. Springer Netherlands,
%			2007.}
%	\end{itemize}
%	
%\end{frame}

\section{Numerical Examples}

\begin{frame}
	\frametitle{Numerical Examples - TTC-based Triggering}

	\hspace*{.5cm}
	\begin{itemize}\setlength{\itemsep}{.3cm}
		\item (at most) $26$ system simulations for\\determining ${\mathcal{I}_A}$ and ${\mathcal{I}_B}$
		\item Evaluating the $\beta_{\text{w},n}$ for $n=0..25$ takes $648$ evaluations\\ of the TTC-based triggering function using the\\ gradient-based solver \texttt{fmincon} in MATLAB
		\item Applying \eqref{eq:owcdProbProd} yields $\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})\approx0.994$\\which is exact since TTC-based has linear boundaries
		\item Non-optimized MC requires $4\cdot10^6$ system simulations\\ to obtain this result with a confidence interval of $\pm10^{-4}$\\ and confidence level of $99\%$\footnote{H. E. Graeb, Analog Design Centering and Sizing. Springer Netherlands, 2007.}\\(interval justified on next slide!)
	\end{itemize}

	\vspace{-5cm}
	\hspace*{9cm}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{|c|c|}
		\hline
		$\sigma_x$                             & $7.24{\text{cm}}$                    \\ \hline
		{$\sigma_v$}       & $14.4\frac{{\text{cm}}}{{\text{s}}}$ \\ \hline 
		{$\tau$}           & $0.533{\text{s}}$                    \\ \hline 
		{$f_{\text{s}}$}   & $50{\text{Hz}}$                      \\ \hline 
		{$x_0$}            & $10{\text{m}}$                       \\ \hline 
		{$v_0$}            & $-10\frac{{\text{m}}}{{\text{s}}}$   \\ \hline 
		{$a$}              & $10\frac{{\text{m}}}{{\text{s}}^2}$  \\ \hline 
		{$x_{\text{min}}$} & $0{\text{m}}$                        \\ \hline 
		{$x_{\text{max}}$} & $0.5{\text{m}}$\\\hline                    
	\end{tabular}
	\renewcommand{\arraystretch}{1}
	
	
	
		
%		\begin{tabular}{c|c|c|c|c}
%			$x_0$&$v_0$&$a$&$x_{\text{min}}$&$x_{\text{max}}$\\\hline
%			$10{\text{m}}$&$-10\frac{{\text{m}}}{{\text{s}}}$&$10\frac{{\text{m}}}{{\text{s}}^2}$&$0{\text{m}}$&$0.5{\text{m}}$
%		\end{tabular}
	
\end{frame}

\begin{frame}
	\frametitle{Numerical Examples - BTN-based Triggering}
	
	\hspace*{.5cm}
	\begin{itemize}\setlength{\itemsep}{.3cm}
		\item (at most) $26$ system simulations for\\determining ${\mathcal{I}_A}$ and ${\mathcal{I}_B}$
		\item Evaluating the $\beta_{\text{w},n}$ for $n=0..25$ takes $1263$ evaluations\\ of the BTN-based triggering function again using\\ \texttt{fmincon} in MATLAB
		\item Applying \eqref{eq:owcdProbProd} yields $\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})\approx0.99722$\\which is off by $3.47\cdot10^{-5}$ compared to ground truth\\
		\item Non-optimized MC requires $2\cdot10^6$ system simulations\\ to obtain this result with a confidence interval of $\pm10^{-4}$\\ and confidence level of $99\%$. Interval chosen to contain\\error of the OWCD result
	\end{itemize}
	
	\vspace{-5cm}
	\hspace*{9cm}
	\renewcommand{\arraystretch}{1.2}
	\begin{tabular}{|c|c|}
		\hline
		$\sigma_x$                             & $7.24{\text{cm}}$                    \\ \hline
		{$\sigma_v$}       & $7.24\frac{{\text{cm}}}{{\text{s}}}$ \\ \hline 
		{$\tau$}           & $9.34\frac{{\text{m}}}{{\text{s}}^2}$                    \\ \hline 
		{$f_{\text{s}}$}   & $50{\text{Hz}}$                      \\ \hline 
		{$x_0$}            & $10{\text{m}}$                       \\ \hline 
		{$v_0$}            & $-10\frac{{\text{m}}}{{\text{s}}}$   \\ \hline 
		{$a$}              & $10\frac{{\text{m}}}{{\text{s}}^2}$  \\ \hline 
		{$x_{\text{min}}$} & $0{\text{m}}$                        \\ \hline 
		{$x_{\text{max}}$} & $0.5{\text{m}}$\\\hline                    
	\end{tabular}
	\renewcommand{\arraystretch}{1}
	
	
	
	
	%		\begin{tabular}{c|c|c|c|c}
	%			$x_0$&$v_0$&$a$&$x_{\text{min}}$&$x_{\text{max}}$\\\hline
	%			$10{\text{m}}$&$-10\frac{{\text{m}}}{{\text{s}}}$&$10\frac{{\text{m}}}{{\text{s}}^2}$&$0{\text{m}}$&$0.5{\text{m}}$
	%		\end{tabular}
	
\end{frame}

\section{Conclusion}

\begin{frame}
	\frametitle{Conclusion}
	\vspace{.2cm}

		The presented OWCD approach 
		\vspace{.1cm}
		\begin{itemize}
			\item is a divide-and-conquer method based on WCD approach from integrated circuit design
			\item efficiently approximates the probability of a vehicular safety system fulfilling its performance specification
			\item is limited in accuracy by non-linearities in decision boundaries
			\item works under the assumption of independence of errors between time steps.
		\end{itemize}

		
		\vspace{.5cm}
		
		More research has to be done regarding
		\begin{itemize}
			\item further uncertainties after triggering an intervention (e.g., random braking deceleration)
			\item effects of stochastic dependence between measurement errors of different time steps.
		\end{itemize}

\end{frame}

\begin{frame}
	\frametitle{Sources}
	[1] C. Stöckle, W. Utschick, S. Herrmann, and T. Dirndorfer, “Robust
	design of an automatic emergency braking system considering sensor
	measurement errors,” 21st IEEE International Conference on Intelligent
	Transportation Systems (ITSC), pp. 2018–2023, Nov 2018.\\
	\bigskip
	[2] H. E. Graeb, C. U. Wieser, and K. J. Antreich, ''Improved methods for worst-case analysis and optimization incorporating operating tolerances,'' 30th ACM/IEEE Design Automation Conference, pp. 142–147, June 1993.\\
	\bigskip
	[3] H. E. Graeb, Analog Design Centering and Sizing. Springer Netherlands, 2007.
\end{frame}


\end{document}