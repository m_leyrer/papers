\documentclass[letterpaper, 10 pt, conference]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgf}
\usepackage{cite}
\usepackage{subcaption}

%\overrideIEEEmargins 


\date{\today}

\begin{document}

	\title{\vspace{.5cm}An Efficient Approach to Simulation-Based Robust Function and Sensor Design Applied to an Automatic Emergency Braking System}
	\author{\IEEEauthorblockN{Michael~L.~Leyrer$^1$, Christoph~St\"ockle$^{1,2}$, Stephan~Herrmann$^2$, Tobias~Dirndorfer$^2$ and Wolfgang~Utschick$^1$}
		\IEEEauthorblockA{$^1$Methods of Signal Processing, Technische Universit\"at M\"unchen, 80290 Munich, Germany\\
			$^2$AUDI AG, 85045 Ingolstadt, Germany\\
			Email: \{michael.leyrer, christoph.stoeckle, utschick\}@tum.de, \{stephan.herrmann, tobias.dirndorfer\}@audi.de}
	}
	
	\maketitle
	
	\begin{abstract}
		Vehicular safety functions can increase automotive safety by intervening in dangerous situations. However, as such functions rely on sensor measurements to decide actions, they are subject to sensor measurement errors which influence the performance. Therefore, a manufacturer has to design both the sensors and functions in a robust manner considering these errors. A methodology for such a robust design has already been proposed for an automatic emergency braking~(AEB) system and is based on a probabilistic quality measure. It is often only possible to evaluate such a probabilistic quality measure through simulations of the system under design. Therefore, a novel approach for efficiently evaluating the probabilistic quality measure through simulations of the AEB system is proposed. The structure of the stochastic problem is analyzed and the new approach derived accordingly. Numerical examples illustrate the savings in computational effort as compared to a Monte Carlo simulation and the accuracy limits. Moreover, the proposed approach generalizes to other vehicular safety systems as well.
	\end{abstract}

	\section{Introduction}\label{s:intro}
	
	In order to improve automotive safety, designers in the automobile industry implement vehicular safety functions which {can} reduce both the number and the severity of collisions. They use sensor measurements for interpreting the current driving situation and deciding an appropriate intervention if necessary. As a consequence, the performance of these systems is influenced by imperfect sensors and unavoidable sensor measurement errors. Therefore, a vehicular safety system has to be designed in a robust manner such that it reliably fulfills a given performance specification for customer satisfaction despite these errors.\\
	
	Capturing the robust design task of an automatic emergency braking~(AEB) system mathematically, \cite{StoeckleAEBOpt} formulates an optimization problem which uses a probabilistic quality measure {reflecting the system performance}. It is {a special case of the general methodology for the robust design of automated vehicular safety systems described in \cite{StoeckleSPM}, which is }based on the worst-case design approach for integrated circuits~(IC) presented in \cite{UtschickSQTol,GraebWCAPract,GraebWCACircuitAnalysis,GraebWCAImp,Graeb2007} and capable of determining the optimal sensor and function parameters with respect to the probabilistic quality measure given a required minimum quality level. This framework is applied in \cite{StoeckleAEBApp} to design an AEB system for several driving scenarios with different decision rules, the best of which with respect to the probabilistic quality measure is selected to be used by the function for triggering an intervention. {While \cite{StelletAnalyticPerf} derives the performance of an AEB system regarding random errors analytically,} it is often only possible to evaluate {it} through simulations of the system under design. One possibility is to use a Monte Carlo~(MC) simulation in order to obtain an approximate value for it. For example, \cite{SafetySystemsMCImpact} investigates the effect of sensor measurement	errors on the uncertainty of collision warning criteria used in
	collision warning systems and \cite{SafetySystemsMCImpactParam} analyzes their influence on	the accuracy of predicted collision parameters like the time-to-collision (TTC) used in predictive passive safety systems based on MC simulations. While it is a very generic method, the MC simulation {in its basic form} has the grave disadvantage that it requires large amounts of computational effort, if an accurate approximation is required.\\
	
	In this paper, an approach for evaluating the probabilistic quality measure, that requires less computational effort than the MC method {as simulation inputs are not just random samples but are cleverly chosen to solve an optimization problem}, while sacrificing little accuracy, is proposed. It is based on {geometric yield analysis employing} the worst-case distance~(WCD) {- a concept} {from worst-case analysis~(WCA)} {that quantifies the essence of a high-dimensional stochastic scenario in a single scalar quantity and is} used in \cite{UtschickSQTol,GraebWCAPract,GraebWCACircuitAnalysis,GraebWCAImp,Graeb2007} for IC design {-} and exploits the structure of the stochastic problem at hand to allow for simplifications. {\cite{NilssonWCA} and \cite{NilssonWCPerf} also use WCA to investigate the performance boundaries of vehicular safety systems regarding systematic errors.}\\
	
	This paper is organized as follows. First, Section~\ref{s:model} introduces the system model and Section~\ref{s:task} the robust design task which are used to derive and evaluate the new approach. In Section~\ref{s:owcd}, the structure of the problem of evaluating the probabilistic quality measure is analyzed and the new approach to it, {named} orthogonal worst-case distance~(OWCD) approach, is derived. Section~\ref{s:ex} gives numerical examples of the application of the OWCD approach in order to illustrate the potential savings in computational effort and the cutbacks in accuracy. Finally, the paper is concluded in Section~\ref{s:conc}.\\
	
	Throughout the paper the following notation is used. The boundary and the complement of a set $\mathcal{A}$ are denoted as $\partial\mathcal{A}$ and $\overline{\mathcal{A}}$, respectively. The probability of an event $A$ is written as $\mathsf{P}\left(A\right)$.  $\mathcal{N}\left(\boldsymbol{\mu},\boldsymbol{C}\right)$ denotes the normal distribution with mean vector $\boldsymbol{\mu}$ and covariance matrix $\boldsymbol{C}$. Furthermore, the cumulative distribution function~(cdf) of the {univariate} standard normal distribution $\mathcal{N}\left(0,1\right)$ is called $\operatorname{\Phi}$.\\
	 
	\section{System Model}\label{s:model}
	
	\subsection{Considered Scenario}
	
	\begin{figure}
		\centering
		\begin{tikzpicture}
		\draw[->] (-2.1,0) -- (6.5,0);
		\fill[violet!30] (1.45,0) rectangle (3.2,1);
		\draw[violet,<->,thick] (1.45,0) -- (3.2,0);
		\draw[dashed] (0,-0.1) node[below]{$x_\text{ego}\left(t\right)$} -- (0,1);
		\draw[dashed] (3.25,-0.1) node[below]{$x_\text{obj}\left(t\right)$} -- (3.25,1);
		\draw[->] (0,0.5) -- ++(0.3,0) node[right]{$v_\text{ego}\left(t\right)$};
		\draw[->] (5.25,0.5) -- ++(0.15,0) node[right]{$v_\text{obj}\left(t\right)$};
		\node[inner sep=0pt] (ego-vecicle) at (-1,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_blue.pdf}};
		\node[inner sep=0pt] (object) at (4.25,0.5) {\includegraphics[width=2cm]{LaTeX-Template_Figures/Figures/Audi_A3_Sedan_red.pdf}};
		\node[above] at (-1,1) {ego vehicle};
		\node[above] at (4.25,1) {object};
		\node[align=center,violet] at (2.325,0.5) {acceptable\\interval};
		\end{tikzpicture}
		\caption{Considered scenario at time $t$.}
		\label{fig:testScenario}
	\end{figure}

	As in \cite{StoeckleAEBOpt,StoeckleAEBApp}, we consider the scenario illustrated in Figure~\ref{fig:testScenario}, with the ego vehicle approaching an object described by a one-dimensional motion model. At a time $t\geq t_0$ the front of the ego vehicle has position $x_{\text{ego}}(t)$ and velocity $v_{\text{ego}}(t)$ while the rear of the object has position $x_{\text{obj}}(t)$ and velocity $v_{\text{obj}}(t)$ in this scenario starting at time $t_0=0$. The object velocity is assumed to be constant over time. If $x_{\text{ego}}(0)< x_{\text{obj}}(0)$, $v_{\text{ego}}(0)> v_{\text{obj}}(0)$ and the velocity of the ego vehicle is assumed to be constant as well, a collision would necessarily occur unless the AEB system detects the threat and triggers a braking maneuver with constant deceleration $a>0$ at time $t_{\text{b}}\geq 0$. In terms of the imminence of a collision, the system can be sufficiently described using only the distance 
	\begin{align}\label{eq:stateX}
		x(t)&=x_{\text{obj}}(t)-x_{\text{ego}}(t)\\\nonumber
		&=\begin{cases}
			x_0 + v_0t,&t\leq t_{\text{b}}\\
			x_0 + v_0t+\frac{a}{2}(t-t_{\text{b}})^2,&t>t_{\text{b}}
		\end{cases}
	\end{align}
	from the front of the ego vehicle to the rear of the object
	and the relative velocity
	\begin{align}\label{eq:stateV}
		v(t)&=v_{\text{obj}}(t)-v_{\text{ego}}(t)\\\nonumber
		&=\begin{cases}
			v_0,&t\leq t_{\text{b}}\\
			v_0 + a(t-t_{\text{b}}),&t>t_{\text{b}},
		\end{cases}
	\end{align}
	between them, where $x_0 = x(0)$ and $v_0 = v(0)$.
	The quantities of interest can be represented by a single state vector ${\boldsymbol{x}}(t)=[x(t),v(t)]^{\text{T}}$.
	
	\subsection{Model of the AEB System}
	
	

	The general mathematical model of a vehicular safety system is shown in Figure~\ref{fig:safetySystemModel}. The time-continuous state vector ${\boldsymbol{x}}(t)\in \mathbb{R}^N$ is sampled by sensors with a sampling frequency $f_{\text{s}}$ resulting in a time-discrete measurement ${\boldsymbol{y}}[n]={\boldsymbol{x}}[n]+{\boldsymbol{\varepsilon}[n]}$, where ${\boldsymbol{x}}[n]={\boldsymbol{x}}(t_n)$ with $t_n = \frac{n}{f_{\text{s}}}$. The measurement errors ${\boldsymbol{\varepsilon}}[n]$ are assumed to be i.i.d. {for each time step $n$} and follow a zero-mean normal distribution with covariance matrix ${\boldsymbol{C}}={\text{diag}}(\sigma_1^2,...,\sigma_N^2)$ which depends on the standard deviations $\sigma_1,...,\sigma_N$ of the errors collected in the vector ${\boldsymbol{\sigma}}=[\sigma_1,...,\sigma_N]^{\text{T}}$, i.e. ${\boldsymbol{\varepsilon}}[n]\sim {\mathcal{N}}({\boldsymbol{0}},{\boldsymbol{C}})$. Note, that the presented OWCD approach also works for a nondiagonal ${\boldsymbol{C}}${, i.e., $\varepsilon_i[n]$ and $\varepsilon_j[m]$ can be stochastically dependent only if $n=m$,} but will only be demonstrated with diagonal ${\boldsymbol{C}}$ for the sake of simplicity. Based on this measurement, the function $f({\boldsymbol{\cdot}};{\boldsymbol{\tau}}): \mathbb{R}^N\to \{0,1\}$, which depends on a set of parameters ${\boldsymbol{\tau}}\in\mathbb{R}^p$, outputs a scalar value $f({\boldsymbol{y}}[n];{\boldsymbol{\tau}})$ in each time step $n$, which is used to decide for triggering an appropriate intervention for the interpreted driving situation ($f({\boldsymbol{y}}[n];{\boldsymbol{\tau}})=1$) or not ($f({\boldsymbol{y}}[n];{\boldsymbol{\tau}})=0$). The smallest $n$ for which $f({\boldsymbol{y}}[n];{\boldsymbol{\tau}})=1$ is $n_{\text{b}}$, which determines the time instant $t_{\text{b}}=\frac{n_{\text{b}}}{f_{\text{s}}}$ at which the intervention is triggered.\\
	
	In case of an AEB system, ${\boldsymbol{y}}[n]=[\hat{x}[n],\hat{v}[n]]^{\text{T}}$, ${\boldsymbol{x}}[n]=[x(t_n),v(t_n)]^{\text{T}}$, ${\boldsymbol{\varepsilon}}[n]=[\varepsilon_x[n],\varepsilon_v[n]]^{\text{T}}$, ${\boldsymbol{C}}={\text{diag}}(\sigma_x^2,\sigma_v^2)$, ${\boldsymbol{\sigma}}=[\sigma_x,\sigma_v]^{\text{T}}$ and different decision functions $f$ can be used, two of which are considered \cite{StoeckleAEBApp}.
	\begin{align}
		f_{\text{TTC}}({\boldsymbol{y}[n]};\tau)&=\begin{cases}
			1,\quad \hat{x}[n]\leq -\tau \hat{v}[n]\\
			0,\quad {\text{else}},
		\end{cases}\\
		f_{\text{BTN}}({\boldsymbol{y}[n]};\tau)&=\begin{cases}
		1,\quad \hat{x}[n]\leq \frac{\hat{v}^2[n]}{\tau}\\
		0,\quad {\text{else}}.
		\end{cases}
	\end{align}
	The first decision rule is based on the TTC $\frac{\hat{x}[n]}{-\hat{v}[n]}$, which measures how long it would take for a collision to occur at distance $\hat{x}[n]>0$ and constant relative velocity $\hat{v}[n]<0$. If the TTC is not larger than a threshold $\tau$, an intervention is triggered. This condition is equivalent to the condition $\hat{x}[n]\leq -\tau \hat{v}[n]$ and the decision boundary $\hat{x}[n]= -\tau \hat{v}[n]$ is linear in the $\hat{x}[n]$-$\hat{v}[n]$-space.\\
%	Based on the measured TTC, a triggering function
%	\begin{equation}
%		f_{\text{TTC}}(\hat{x}[n],\hat{v}[n],\tau) = \frac{\hat{x}[n]}{-\hat{v}[n]} - \tau
%	\end{equation}
%	can be formulated with a lower threshold value $\tau$, the measured distance $\hat{x}[n]$ and measured relative velocity $\hat{v}[n]$. If $f_{\text{TTC}}(\hat{x}[n],\hat{v}[n],\tau)<0$ at time step $n=n_{\text{b}}$, a braking maneuver will be triggered.
	
	The second decision rule is based on the brake-threat-number~(BTN) $\frac{\hat{v}^2[n]}{2\hat{x}[n]}$, which is the constant deceleration required to just avoid a collision at current distance $\hat{x}[n]>0$ and relative velocity $\hat{v}[n]<0$, i.e., the distance is zero exactly when the relative velocity reaches zero. If the BTN is not smaller than a threshold $\tau$, an intervention is triggered. This condition is equivalent to the condition $\hat{x}[n]\leq \frac{\hat{v}^2[n]}{2\tau}$ and the decision boundary $\hat{x}[n]= \frac{\hat{v}^2[n]}{2\tau}$ is nonlinear in the $\hat{x}[n]$-$\hat{v}[n]$-space in contrast to the TTC-based triggering condition.
%	By solving the equations of motion, it can be derived as
%	\begin{equation}
%		{\text{BTN}}(t) = \frac{v(t)^2}{2x(t)}.
%	\end{equation}
%	Based on the measured BTN, a second triggering function
%	\begin{equation}
%		f_{\text{BTN}}(\hat{x}[n],\hat{v}[n],\tau) = \tau - \frac{\hat{v}[n]^2}{2\hat{x}[n]}
%	\end{equation}
%	can be defined with an upper threshold value $\tau$ and the measured distance $\hat{x}[n]$ and measured relative velocity $\hat{v}[n]$. Equivalently to TTC-based triggering, if $f_{\text{BTN}}(\hat{x}[n],\hat{v}[n],\tau)<0$, an intervention will be triggered at time step $n=n_{\text{b}}$.
%	Unlike for $f_{\text{TTC}}$, the triggering boundary $f_{\text{BTN}}(\hat{x}[n],\hat{v}[n],\tau)=0$ is nonlinear in the $\hat{x}[n]$-$\hat{v}[n]$-space.

	\begin{figure}
		\begin{center}
			\begin{tikzpicture}
			\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,blue] (sensors)  at (1.5,0) {sensors};
			\node[rectangle,draw,text width=1.5cm,minimum height=0.5cm,text centered,red] (function) at (4.5,0) {function};
			
			\draw[-latex] (0,0) node[left]{$\boldsymbol{x}[n]$} -- (sensors);
			\draw[-latex](1.5,-0.75) node[below]{$\boldsymbol{\varepsilon}[n]$} -- (sensors);
			\draw[-latex](sensors) -- node[midway,above]{$\boldsymbol{y}[n]$} (function);
			\draw[-latex](function) -- node[above,midway]{$f(\boldsymbol{y}[n];\boldsymbol{\tau})$} (7.5,0);
			\end{tikzpicture}
		\end{center}
		\caption{General model of a vehicular safety system including sensor measurement errors $\boldsymbol{\varepsilon}[n]$.}
		\label{fig:safetySystemModel}
	\end{figure}
	
	\section{Design Task}\label{s:task}
	
	In general, the task of the robust design of a vehicular safety system can be formulated as follows (cf. \cite{StoeckleAEBOpt,StoeckleAEBApp}):
	\begin{equation}
		({\boldsymbol{\tau}}_{\text{opt}},{\boldsymbol{\sigma}}_{\text{opt}})=\underset{{\boldsymbol{\tau}}\in\mathbb{R}^p,{\boldsymbol{\sigma}}\in\mathbb{R}^N}{\arg\min}\:c({\boldsymbol{\sigma}})\quad \text{s.t.}\quad Q({\boldsymbol{\tau}},{\boldsymbol{\sigma}})\geq Q_{\text{min}}
	\end{equation}
	with a cost function $c({\boldsymbol{\sigma}})$, a quality measure $Q({\boldsymbol{\tau}},{\boldsymbol{\sigma}})$ and a required minimum quality level $Q_{\text{min}}$. In the case of the present AEB system, the quality measure can be defined as the probability
	\begin{equation}
		Q(\tau,\sigma_x,\sigma_v) = \mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})
	\end{equation}
	that the distance $x_{\text{end}}=x(t_{\text{end}})$ after the braking intervention when the relative velocity is $v_{\text{end}}=v(t_{\text{end}})=0$ is within a predefined acceptance interval $[x_{\text{min}},x_{\text{max}}]$. {$x(t)$ and $v(t)$ evolve according to \eqref{eq:stateX} and \eqref{eq:stateV} and all values except $t_b$ are assumed to be deterministic}. $Q$ depends on the threshold value $\tau$ and the standard deviations of the sensor measurement errors $\sigma_x$ and $\sigma_v$. The acceptance interval is also illustrated in Figure~\ref{fig:testScenario}.\\
	
	In this specific, simple scenario and when using TTC-based triggering with $\sigma_v=0$, there actually exists a closed-form expression for $\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})$, which is derived in \cite{StoeckleAEBOpt}. But in general, this probability has to be approximated by simulating the system, e.g., with an MC simulation{, since a closed-form expression does not always exist or is very hard to derive}. In an MC simulation, many samples of the measurement errors, for which the AEB system is then simulated, are drawn independently and according to their distribution. The estimated probability of fulfilling the performance specification is then given by the frequency with which the performance specification is fulfilled. However, an MC simulation {- even with methods like importance sampling \cite{importanceSampling} which reduce computational effort -} requires many samples, i.e., system simulations, when a high precision of this approximation is demanded.
	
	\section{Orthogonal Worst-Case Distance Approach}\label{s:owcd}
	
%	 In order to derive the OWCD approach, the structure of the problem of evaluating $\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})$ is analyzed.

	\subsection{Worst-Case Distance Approach}
	
	The WCD approach for IC design formulated in \cite{UtschickSQTol,GraebWCAPract,GraebWCACircuitAnalysis,GraebWCAImp,Graeb2007} reduces a multi-dimensional integral to the evaluation of the standard normal cdf $\Phi$ at a single scalar quantity, the WCD $\beta_{\text{w}}$. Given statistical parameters ${\boldsymbol{s}}\in \mathbb{R}^N$ with ${\boldsymbol{s}}\sim {\mathcal{N}}({\boldsymbol{s}}_0,{\boldsymbol{C}})$ and an acceptance region ${\mathcal{A}}\subseteq \mathbb{R}^N$ with a smooth boundary $\partial {\mathcal{A}}$, the probability $\mathsf{P}({\boldsymbol{s}}\in{\mathcal{A}})$ of ${\boldsymbol{s}}$ being in the acceptance region ${\mathcal{A}}$ can be approximated as
	\begin{equation}\label{eq:wcdProb}
		\mathsf{P}({\boldsymbol{s}}\in{\mathcal{A}}) \approx \begin{cases}
			\Phi (\beta_{\text{w}}),&\quad {\boldsymbol{s}_0}\in {\mathcal{A}}\\
			1-\Phi (\beta_{\text{w}})=\Phi (-\beta_{\text{w}}),&\quad {\boldsymbol{s}_0}\in {\overline{\mathcal{A}}}.
		\end{cases}
	\end{equation}
	The approximation amounts to a linearization of $\partial {\mathcal{A}}$ in its likeliest point. Intuitively, the WCD is the smallest distance from ${\boldsymbol{s}_0}$ to the boundary $\partial{\mathcal{A}}$ of the acceptance region in terms of the Mahalanobis distance $\beta({\boldsymbol{s}})$, i.e., it represents the smallest deviation of the statistical parameters ${\boldsymbol{s}}$ from their mean ${\boldsymbol{s}_0}$ that is barely acceptable. This can be formulated as
	\begin{equation}\label{eq:wcdDef}
		\beta_{\text{w}}^2=\min_{{\boldsymbol{s}}\in\mathbb{R}^N}\beta^2({\boldsymbol{s}})\quad{\text{s.t.}}\quad {\boldsymbol{s}}\in \partial{\mathcal{A}},
	\end{equation}
	where
	\begin{equation}\label{eq:betaDef}
		\beta^2({\boldsymbol{s}})=({\boldsymbol{s}}-{\boldsymbol{s}}_0)^{\text{T}}{\boldsymbol{C}}^{-1}({\boldsymbol{s}}-{\boldsymbol{s}}_0).
	\end{equation}
	and is illustrated in Figure~\ref{fig:wcdIllust} for the two-dimensional case $N=2$.\\
	
	If \eqref{eq:wcdDef} has to be solved numerically, the constraint ${\boldsymbol{s}}\in \partial{\mathcal{A}}$ would be equivalently reformulated as
	\begin{align}\label{eq:wcdOptConstrNum}
		\boldsymbol{s}\in\nonumber  \partial{\mathcal{A}}\cup\overline{\mathcal{A}},\quad&\text{if}\quad\boldsymbol{s}_0\in \mathcal{A}\\
		\boldsymbol{s}\in \partial{\mathcal{A}} \cup\mathcal{A},\quad&\text{if}\quad\boldsymbol{s}_0\in \overline{\mathcal{A}}
	\end{align}
	for reasons of numerical optimization.
	
	\begin{figure}
		\begin{center}
			\begin{tikzpicture}[scale=.7]
			\draw[->] (-4.5,0)--(4.5,0) node[anchor=west]{$s_1$};
			\draw[->] (0,-4.5)--(0,4.5) node[anchor=west]{$s_2$};
			%		\foreach \x in {-4,...,4}{
			%			\draw (\x,-.1)--(\x,.1)(-.1,\x)--(.1,\x);
			%		}
			\draw[blue, very thick, dashed] (-2,4.5)--(4.5,-.35) (3.5,-3.5)node[]{$\mathcal{A}$}(3.5,3.5)node[]{$\overline{\mathcal{A}}$};
			\draw[blue,very thick](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)node[anchor=west]{$\partial {\mathcal{A}}$};
			\fill[blue, opacity=.1](-.7,4.5) .. controls (.8,2.3) and (2.5,.7) .. (4.5,.5)|-(-4.5,-4.5)|-cycle;
			%\fill[blue, opacity=.1] (-2,4.5)--(4.5,-.35)|-(-4.5,-4.5)|-cycle;
			\foreach \x in {1,2,3}
			\draw[red, thin,dashed,font=\tiny] (0,0) circle[x radius=1.5*\x, y radius=\x] (0,-\x) node[anchor=north east]{$\beta = \x$};
			\draw[red,thick,|-|](0,0)--(2.26,1.33);
			\draw[red](1.4,.5)node[rotate=30]{$\beta_\text{w}=2$};
			\end{tikzpicture}
		\end{center}
		\caption{Illustration of the WCD $\beta_\text{w}$ for $\sigma_1=1.5\sigma_2$ and uncorrelated, zero-mean statistical parameters $s_1$ and $s_2$.}
		\label{fig:wcdIllust}
	\end{figure}
	
	\subsection{Problem Structure}
	
	The WCD approach can directly be applied to the task of evaluating the probabilistic quality measure for the AEB system with the sensor measurement errors as the statistical parameters and by using \eqref{eq:wcdDir}, where $\beta_{\text{w,max}}$ and $\beta_{\text{w,min}}$ are the WCDs of the acceptance regions corresponding to $x_\text{end}\leq x_\text{max}$ and $x_\text{end}\geq x_\text{min}$ respectively.
	\begin{align}\label{eq:wcdDir}
		\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq\nonumber x_{\text{max}})&=\mathsf{P}(x_\text{end}\leq x_\text{max})-\mathsf{P}(x_\text{end}< x_\text{min})\\
		&\approx\Phi(\beta_{\text{w,max}})-\Phi(-\beta_{\text{w,min}})
	\end{align}
	
	While the obtained approximate probability is rather accurate for very small standard deviations of the sensor measurement errors, the WCDs $\beta_{\text{w,min}}$ and $\beta_{\text{w,max}}$ are difficult to determine with numerical methods and the approximation quickly becomes inaccurate for larger standard deviations. The reason for both is, that in this case the acceptance region ${\mathcal{A}}$, which is the set of errors ${\boldsymbol{\varepsilon}[n]}$ in all relevant time steps $n=0,1,...,n_{\text{max}}$ collected in the error vector ${\boldsymbol{\varepsilon}}=[{\boldsymbol{\varepsilon}}^{\text{T}}[0],{\boldsymbol{\varepsilon}}^{\text{T}}[1],...,{\boldsymbol{\varepsilon}}^{\text{T}}[n_{\text{max}}]]^{\text{T}}\in \mathbb{R}^{2(n_{\text{max}}+1)}$ which lead to $x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}}$, has a boundary $\partial{\mathcal{A}}$ which is not smooth and nonlinear. Thus the linear approximation made when using \eqref{eq:wcdProb} is not very accurate and numerical solvers, e.g., \texttt{patternsearch} in MATLAB, tend to get stuck in local minima of \eqref{eq:wcdDef} together with \eqref{eq:wcdOptConstrNum} as the constraint.\\

	Figure~\ref{fig:orthogonalBoundary} illustrates the boundary for a simple exemplary case with TTC-based triggering and only an error in the position measurement, i.e., $\varepsilon_v[n]=0$ for all $n$. The time steps therein are of arbitrary choice, as this examples purpose is to basically illustrate the problem structure.\\
	
	The set $\overline{\mathcal{T}}_n\subseteq\mathbb{R}^{2(n_{\text{max}}+1)}$ is the set the error vector ${\boldsymbol{\varepsilon}}$ has to be in, such that no intervention is triggered in time step $n$.\\
	
		\begin{figure}
		\begin{subfigure}{\linewidth}
			\begin{center}
				\begin{tikzpicture}
				\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x}[3]$};
				\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x}[4]$};
				
				\draw[blue, dashed] (-3.3,-.4)node[anchor=east]{$\partial \mathcal{T}_4$}-|(-1,-3.3)node[anchor=north]{$\partial \mathcal{T}_3$};
				\draw[very thick, blue] (3.3,-.4)-|(-1,3.3);
				\fill[green, opacity=.05] (3.3,-.4)-|(-3.3,3.3)-|cycle;
				\fill[red, opacity=.05] (3.3,-3.3)-|(-1,3.3)-|cycle;
				\fill[blue, opacity=.2] (-1,3.3)|-(3.3,-.4)|-cycle;
				\draw[blue, font=\large] (-2.5,1.7)node[]{$\overline{\mathcal{T}}_{4}$} (1.5,-2.5)node[]{$\overline{\mathcal{T}}_3$} (1.7,1.7)node[]{$\overline{\mathcal{T}}_3\cap \overline{\mathcal{T}}_{4}\supseteq \mathcal{A}$};
				%				\draw[red, thick,|-|] (1,-.4)--(1,0)node[anchor= south]{$\beta_{\text{w,max}}$};
				\draw[black,thick](-1,-.1)-|(-.7,-.4);
				\draw[thick,red,<->](-1,1)--(0,1)node[midway,anchor=south]{$\beta_{\text{w},3}$};
				\draw[thick,red,<->](1,0)--(1,-.4)node[midway,anchor=west]{$\beta_{\text{w},4}$};
%				\draw[black, thick](-1,-.1)..controls(-.8,-.1)and(-.7,-.2)..(-.7,-.4)(-.88,-.3)node[]{$\boldsymbol{\cdot}$};
				\end{tikzpicture}
			\end{center}
			\caption{Time steps $3$ and $4$ at which no intervention must be triggered.}
			\label{fig:orthogonalBoundary:a}
			\vspace{1cm}
		\end{subfigure}
		
		\begin{subfigure}{\linewidth}
			\begin{center}
				\begin{tikzpicture}
				\draw[->] (-3.5,0)--(3.5,0) node[anchor=west]{$\varepsilon_{x}[5]$};
				\draw[->] (0,-3.5)--(0,3.5) node[anchor=west]{$\varepsilon_{x}[6]$};
				\draw[very thick, blue] (.2,3.3)|-(3.3,.8);
				\draw[blue, dashed] (.2,-3.3)node[anchor=north]{$\:\,\partial \mathcal{T}_5$}|-(-3.3,.8)node[anchor=east]{$\partial \mathcal{T}_6$};
				\fill[blue, opacity=.2](-3.3,3.3)-|(.2,.8)-|(3.3,-3.3)-|cycle;
				\fill[green,opacity=.05](-3.3,3.3)|-(3.3,.8)|-cycle;
				\fill[red,opacity=.05](.2,3.3)|-(3.3,-3.3)|-cycle;
				\draw[blue, font=\large] (-2,2.2)node[]{$\overline{\mathcal{T}}_{6}$} (1.7,-2) node[]{$\overline{\mathcal{T}}_5$} (-1.7,-1.7)node[]{$\overline{(\overline{\mathcal{T}}_5\cap \overline{\mathcal{T}}_6)} \supseteq \mathcal{A}$};
				%				\draw[red, thick,|-|] (.2,.8)--(0,0)node[anchor= south east]{$\beta_{\text{w,min}}$};
				\draw[blue, font=\large](1.7,1.7)node[]{$\overline{\mathcal{T}}_{5}\cap \overline{\mathcal{T}}_{6}$};
				\draw[black,thick](.2,1.1)-|(.5,.8);
				\draw[thick,red,<->](.2,1.2)--(0,1.2)node[anchor=east]{$\beta_{\text{w},5}$};
				\draw[thick,red,<->](1,0)--(1,.8)node[midway,anchor=west]{$\beta_{\text{w},6}$};
%				\draw[black, thick](.2,1.1)..controls(.4,1.1)and(.5,1)..(.5,.8)(.32,.9)node[]{$\boldsymbol{\cdot}$};
				\end{tikzpicture}
			\end{center}
			\caption{Time steps $5$ and $6$ at which an intervention must be triggered at least once.}
			\label{fig:orthogonalBoundary:b}
		\end{subfigure}
		\caption{Cross-sections of an exemplary acceptance region $\mathcal{A}$ in the measurement error space for TTC-based triggering and $\varepsilon_{v}[n]=0$.}
		\label{fig:orthogonalBoundary}
	\end{figure}
	
	In time steps $3$ and $4$ (and those before), no intervention must be triggered in order to fulfill $x_{\text{end}}\leq x_{\text{max}}$, i.e., ${\boldsymbol{\varepsilon}}$ must lie within the overlap of $\overline{\mathcal{T}}_3$ and $\overline{\mathcal{T}}_4$ shown in Figure~\ref{fig:orthogonalBoundary:a}.
	Conversely, an intervention must be triggered in time steps $5$ or $6$ in order to fulfill $x_{\text{end}}\geq x_{\text{min}}$. This is equivalent to ${\boldsymbol{\varepsilon}}$ being in the union of $\mathcal{T}_5$ and $\mathcal{T}_6$, i.e., not being in the overlap of $\overline{\mathcal{T}}_5$ and $\overline{\mathcal{T}}_6$, as is shown in Figure~\ref{fig:orthogonalBoundary:b}.\\
	
	
	

	
	Also, the boundaries $\partial \mathcal{T}_n$ between $\mathcal{T}_n$ and $\overline{\mathcal{T}}_n$ are clearly orthogonal to each other, i.e. they intersect each other at an angle of $90^{\text{o}}$. This is due to the independence of the decisions at different time steps $n$. Those orthogonal pieces, which construct the boundary $\partial {\mathcal{A}}$, can in general be linear or nonlinear depending on the decision rule $f({\boldsymbol{\cdot}};{\boldsymbol{\tau}})$.\\
	
	
	
	\subsection{Orthogonal Worst-Case Distance Approach} 
	
	By {only considering the errors in one} time step $n$ {individually}, the WCD approach~\eqref{eq:wcdProb} can be applied for each time step $n$ in order to approximate the probability $\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)$ of not triggering an intervention in time step $n$:
	\begin{equation}\label{eq:owcdProb}
		\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)\approx\begin{cases}
			\Phi(\beta_{\text{w},n}),&\quad{\boldsymbol{0}}\in \overline{\mathcal{T}}_n\\
			\Phi(-\beta_{\text{w},n}),&\quad{\boldsymbol{0}}\in\mathcal{T}_n
		\end{cases}
	\end{equation}
	For the AEB system, the WCDs $\beta_{\text{w},n}$ can be evaluated by
	\begin{align}
		\beta_{\text{w},n}^2 = \min_{{\boldsymbol{\varepsilon}[n]}\in\mathbb{R}^2}\beta^2({\boldsymbol{\varepsilon}[n]}),\quad{\text{s.t.}}\quad{\boldsymbol{\varepsilon}[n]}\in \partial {\mathcal{T}}_n,
	\end{align}
	where
	\begin{equation}
		\partial {\mathcal{T}_n} = \begin{cases}
			\{\boldsymbol{\varepsilon}\in \mathbb{R}^{2(n_{\text{max}}+1)}:\hat{x}[n]=-\tau \hat{v}[n]\},\!\!\!&f\!=\! f_{\text{TTC}}\\
			\{\boldsymbol{\varepsilon}\in \mathbb{R}^{2(n_{\text{max}}+1)}:\hat{x}[n]=\frac{\hat{v}[n]^2}{2\tau}\},&f\!=\!f_{\text{BTN}}.
		\end{cases}
	\end{equation}
	As for single time steps $n$ the boundaries $\partial \mathcal{T}_n$ are smooth and, depending on the decision rule $f({\boldsymbol{\cdot}};{\boldsymbol{\tau}})$, nonlinear or linear, the WCD approach gives an accurate or even perfect result. Additionally, the task of evaluating the WCDs is much easier in this case. The remaining task is to compute the approximate probability of fulfilling the performance specification from the obtained approximate probabilities ${\mathsf{P}}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)$ for all relevant time steps $n$. Let us therefore consider the scenario again. In order to have $x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}}$, one must not trigger a braking maneuver too early to avoid $x_{\text{end}}>x_{\text{max}}$, i.e., the function must not trigger for $n<n_{\text{min}} \iff n\in {\mathcal{I}}_A=\{0,1,...,n_{\text{min}}-1\}$, where $n_{\text{min}}$ can be determined by simulation and is independent of the function and sensor parameters. Thus it only has to be determined once for each scenario. On the other hand one must also trigger a braking maneuver in time to satisfy $x_{\text{end}}\geq x_{\text{min}}$, i.e., the function must trigger at least once for $n_{\text{min}}\leq n\leq n_{\text{max}}\iff n\in {\mathcal{I}}_B=\{n_{\text{min}},n_{\text{min}}+1,...,n_{\text{max}}\}$, where $n_{\text{max}}$ can be determined similarly to $n_{\text{min}}$.\\
	
	With this analysis of the scenario, the probability of fulfilling the performance specification can be reformulated as an expression depending on the individual $\mathsf{P}({\boldsymbol{\varepsilon}}\in\overline{\mathcal{T}}_n)$:
	\begin{align}
		\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})=\mathsf{P}\left({\boldsymbol{\varepsilon}}\in\underset{n\in{\mathcal{I}_A}}{\bigcap}\overline{\mathcal{T}}_n \cap \underset{n\in{\mathcal{I}_B}}{\bigcup}\mathcal{T}_n\right)\nonumber\\
		=\left(\prod_{n\in{\mathcal{I}_A}}\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)\right)\left(1-\prod_{n\in{\mathcal{I}_B}}\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)\right)\label{eq:owcdProbProd}
	\end{align}
	The first line is just the statement of the above paragraph written formally. Using $\mathsf{P}(A\cap B)=\mathsf{P}(A)\mathsf{P}(B)$ if and only if the events $A$ and $B$ are stochastically independent and the stochastic independence of the events ${\boldsymbol{\varepsilon}}\in\overline{\mathcal{T}}_n$ for different $n$, the second line can be derived.\\
	
	Using the orthogonality of the triggering decision boundaries $\partial{\mathcal{T}_n}$ in different time steps $n$, which matches the stochastic independence of the events ${\boldsymbol{\varepsilon}}\in \overline{\mathcal{T}}_n$, this accordingly called orthogonal WCD~(OWCD) approach can be used to get an efficient and accurate approximation for $\mathsf{P}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})$.
	
	\section{Numerical Examples}\label{s:ex}
	
	In order to illustrate some advantages of the OWCD approach, numerical examples including ones with linear and nonlinear decision rules are presented below. The performance of the OWCD approach is compared to an MC simulation of the system.
	
	\subsection{Linear TTC-Based Triggering}
	
	Using the TTC-based triggering function and independent sensor measurement errors for both the relative position and the relative velocity, the OWCD approach is applied in order to evaluate the probability that the AEB system fulfills the performance specification for the parameters and specifications given in Table~\ref{tab:TTCparams}.\\
	
	This particular combination of sampling frequency and initial conditions for the simulation and gives $26$ time steps of interest as $n_{\text{max}}=25$. Thus, $26$ simulations of the system are required in order to determine the index sets ${\mathcal{I}_A}$ and ${\mathcal{I}_B}$. After this is done once, the performance of the system can be evaluated for arbitrary $\sigma_x$, $\sigma_v$ and $\tau$. For the given system parameters, evaluating the WCDs for all necessary time steps takes a total of $648$ evaluations of the TTC-based triggering function using the gradient based solver \texttt{fmincon} in MATLAB. Calculating the approximate probability of fulfilling the performance specification according to \eqref{eq:owcdProb}-\eqref{eq:owcdProbProd} yields ${\mathsf{P}}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}}) \approx 0.994$, which is in this case the exact result due to the linear boundary for TTC-based triggering. A MC simulation requires $4\cdot10^6$ full simulations of the system in order to obtain this probability within a confidence interval $[0.994-10^{-4}, 0.994+10^{-4}]$ at confidence level $99\%$ \cite[Eq. (232)]{Graeb2007}. This narrow confidence interval will be justified in the following subsection as the result from the OWCD approach is exact for TTC-based triggering and thus not well comparable in terms of the error.
	
	\begin{table}
		\caption{System and simulation parameters for TTC-based example and performance specification.}
		\label{tab:TTCparams}
		\renewcommand{\arraystretch}{1.5}
		\centering
		\begin{tabular}{c|c|c|c}
			$\sigma_x$&$\sigma_v$&$\tau$&$f_{\text{s}}$\\\hline
			$7.24{\text{cm}}$&$14.4\frac{{\text{cm}}}{{\text{s}}}$&$0.533{\text{s}}$&$50{\text{Hz}}$
		\end{tabular}\vspace{.5cm}\\
		\begin{tabular}{c|c|c|c|c}
			$x_0$&$v_0$&$a$&$x_{\text{min}}$&$x_{\text{max}}$\\\hline
			$10{\text{m}}$&$-10\frac{{\text{m}}}{{\text{s}}}$&$10\frac{{\text{m}}}{{\text{s}}^2}$&$0{\text{m}}$&$0.5{\text{m}}$
		\end{tabular}
		\renewcommand{\arraystretch}{1}
	\end{table}
	
	\subsection{Nonlinear BTN-Based Triggering}
	
	The same procedure is applied to an AEB system with the BTN-based triggering function. The parameters and specifications can be found in Table~\ref{tab:BTNparams}. In contrast to TTC-based triggering, there is no closed-form expression for ${\mathsf{P}}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}})$ if BTN-based triggering is used. For an alternative ground truth, a large scale MC sampling was performed on the BTN-based triggering function, in order to obtain very precise estimations for the $\mathsf{P}({\boldsymbol{\varepsilon}\in}\overline{\mathcal{T}}_n)$ which were then used in \eqref{eq:owcdProbProd}.\\
	
	Again, 26 simulations of the system are required in order to determine the index sets ${\mathcal{I}_A}$ and ${\mathcal{I}_B}$, as both the performance specification and the simulation parameters are the same as before. Evaluating the $26$ necessary WCDs using \texttt{fmincon} takes a total of $1263$ evaluations of the BTN-based triggering function which is about twice as much as for TTC-based triggering. Together with \eqref{eq:owcdProb}-\eqref{eq:owcdProbProd} this yields an approximate probability of fulfilling the performance specification ${\mathsf{P}}(x_{\text{min}}\leq x_{\text{end}}\leq x_{\text{max}}) \approx 0.99722$. Due to the nonlinearity of the BTN decision boundary, \eqref{eq:owcdProb} is now really an approximation which leads to an error of $3.47\cdot10^{-5}$ compared to the ground truth. This error is the reason why a confidence range of $\pm 10^{-4}$ for the comparison to an MC simulation was chosen above. Similar to before, $2\cdot10^6$ full system simulations are required for an MC simulation to obtain the same probability within this confidence range at confidence level $99\%$ \cite[Eq. (232)]{Graeb2007}.
	
	\begin{table}
		\caption{System and simulation parameters for BTN-based example and performance specification.}
		\label{tab:BTNparams}
		\renewcommand{\arraystretch}{1.5}
		\centering
		\begin{tabular}{c|c|c|c}
			$\sigma_x$&$\sigma_v$&$\tau$&$f_{\text{s}}$\\\hline
			$7.24{\text{cm}}$&$7.24\frac{{\text{cm}}}{{\text{s}}}$&$9.43\frac{{\text{m}}}{{\text{s}}^2}$&$50{\text{Hz}}$
		\end{tabular}\vspace{.5cm}\\
		\begin{tabular}{c|c|c|c|c}
			$x_0$&$v_0$&$a$&$x_{\text{min}}$&$x_{\text{max}}$\\\hline
			$10{\text{m}}$&$-10\frac{{\text{m}}}{{\text{s}}}$&$10\frac{{\text{m}}}{{\text{s}}^2}$&$0{\text{m}}$&$0.5{\text{m}}$
		\end{tabular}
		\renewcommand{\arraystretch}{1}
	\end{table}
	
	\section{Conclusion}\label{s:conc}
	
	The presented OWCD approach is a divide-and-conquer-like method based on the WCD approach from IC design and can be used in order to more efficiently evaluate the probability that a vehicular safety system fulfills its performance specification. This was illustrated through the example of a simple AEB system with sensor measurement errors in relative position and relative velocity. While this is not a very realistic test scenario and only a proof of concept, it seems promising that the OWCD approach becomes a useful tool in the simulation-based robust design of vehicular safety systems.\\
	
	Compared to methods like Monte Carlo simulation, it offers a significant reduction in computational effort, while sacrificing very little accuracy. On the downside, the computational effort of the OWCD approach scales with the number of simulation steps and the complexity of the used decision rules. Additionally, it is based on a few vital assumptions such as having independent measurement errors in different time steps or having fully deterministic behavior after the safety function has triggered an intervention. Evaluating and handling the effects of violating some of those assumptions will require further research.\\
	
	
	
	
	\bibliographystyle{IEEEtran}
	\bibliography{owcdInRobustDesign}
		
\end{document}