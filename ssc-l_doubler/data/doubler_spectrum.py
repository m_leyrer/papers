import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.special import lambertw

alpha = 5.9
n = 2.2

db20 = lambda x: 20*np.log10(np.abs(x))

t = np.linspace(0,2*3.14159265,45)[:-1]
f = np.linspace(0,43,44)
y = 2*n*26e-3*lambertw(alpha*np.cosh(100/26/n*np.cos(t-0.5*3.141592)))
y = -y + (np.max(y)+np.min(y))/2

data = np.genfromtxt("doubler_output-t_in100mV_f10M.csv", skip_header=1, delimiter=',')
x2 = data[:-1,0]
x2 = x2*t[-1]/x2[-1]
y2 = data[:-1,1]

Y = np.fft.fft(y)
Y2 = np.fft.fft(y2)

plt.figure(0)
ax = plt.subplot(1,1,1)
plt.plot(f[1:5],db20(Y[1:5])-db20(Y[2]),label='model',marker='x',linewidth=0)
plt.plot(f[1:5],db20(Y2[1:5])-db20(Y2[2]),label='simulation',marker='+',linewidth=0)
ax.legend()
plt.xlabel('$f$ normalized')
plt.ylabel('$V_{\mathrm{out}}$ in dBV')
plt.grid(b=True,which='both')
ax.get_yaxis().set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.get_xaxis().set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid(b=True, which='minor', linewidth=0.2)

np.savetxt('model_vs_sim_spectrum.csv', np.array([f[1:5],db20(Y[1:5])-db20(Y[2]),f[1:5],db20(Y2[1:5])-db20(Y2[2])]).transpose(), header='f,model,f,sim', delimiter=',')

plt.show()