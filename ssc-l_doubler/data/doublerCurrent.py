import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.special import lambertw

alpha = 5.9
n = 2.2

t = np.linspace(0,2*3.14159265,45)
y = 2*n*26e-3*lambertw(alpha*np.cosh(100/26/n*np.cos(t-0.5*3.141592)))
y = -y + (np.max(y)+np.min(y))/2

data = np.genfromtxt("doubler_output-t_in100mV_f10M.csv", skip_header=1, delimiter=',')
x2 = data[:,0]
x2 = x2*t[-1]/x2[-1]
y2 = data[:,1]
y2 = y2 - (np.max(y2)+np.min(y2))/2

plt.figure(0)
ax = plt.subplot(1,1,1)
plt.plot(t,y,label='model')
plt.plot(x2,y2,label='simulation')
ax.legend()
plt.xlabel('$t$ in ${2\\pi}/{f}$')
plt.ylabel('$v_{out}$ in V')
plt.grid(b=True,which='both')
ax.get_yaxis().set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.get_xaxis().set_minor_locator(mpl.ticker.AutoMinorLocator())
ax.grid(b=True, which='minor', linewidth=0.2)


np.savetxt('model_vs_sim_td.csv', np.real(np.array([t/2*3.14159265,y,t/2*3.14159265,y2])).transpose(), header='t/T_0,model,t/T_0,sim', delimiter=',')

plt.show()