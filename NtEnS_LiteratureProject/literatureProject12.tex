\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\usepackage{tikz}
\usepackage{subcaption}


\begin{document}

\title{On Efficient Thermoelectric Devices Based on Graphene Superlattices}

\author{\IEEEauthorblockN{Michael L. Leyrer (michael.leyrer@tum.de)}
\IEEEauthorblockA{\textit{TU Munich, Arcisstr. 21 80333 Munich, Germany}}

}

\maketitle

\begin{abstract}
Thermoelectric devices (TED) can be used to convert heat energy into electrical energy or vice versa. They can thus be used for electric power generation or cooling applications, possibly replacing current means of power generation and refrigeration. Since bulk material based TEDs are not efficient enough to be feasible for such tasks, a lot of research is being done on TEDs based on different nanostructures which can exceed the apparent limits of bulk materials. This paper aims to explain the origin of the strong thermoelectric effect in graphene superlattices and to summarize research regarding TEDs based on theses.
\end{abstract}

\begin{IEEEkeywords}
thermoelectricity, graphene superlattice
\end{IEEEkeywords}

\section{Introduction}\label{s:intro}
The efficiency of TEDs is governed by the dimensionless thermoelectric figure of merit~(FOM) $ZT$. In order to practically replace current technology such as silicon solar cells for power generation, $ZT\gtrsim 3$ is required. While the value of $ZT$ initially seemed to never exceed $1$ since there exist rather unfortunate correlations between the factors of $ZT$ in bulk materials such as silicon, recent advances had been made using more complex bulk materials, nanomaterials or nanostructuring. For a review of the development of the FOM, see \cite{alam_FOM_review}.\\

One very promising nanomaterial is \textit{graphene} which is a one atom thick sheet of carbon atoms arranged in a hexagonal structure. Sarma \textit{et al.} \cite{sarma_graphene_basics} cover electronic transport in graphene and some effects that emerge, such as the Klein paradox (see \cite{klein_paradox}), in great detail.\\

Indeed, a very strong thermoelectric effect has been observed by Mishra \textit{et al.}\cite{mishra_main} Dragoman \textit{et al.}\cite{dragoman_main} in graphene superlattices, which are graphene lattices with an additional superimposed structure, e.g., doped areas in the graphene or external electric fields. This strong thermoelectric effect is produced by a mechanism referred to as \textit{resonant tunnelling} where the transmission coefficient for electron transport through a structure has a very sharp peak at a certain energy level (see \cite{zhang_resonant_tunnelling}) which is closely related to the above mentioned Klein paradox (see \cite{katsnelson_tunnelling_klein}).\\

This paper is organized as follows: Section~\ref{s:tebasics} introduces the fundamentals of thermoelectricity which are required to understand the working principles behind graphene superlattice based TEDs which are explained in Section~\ref{s:gslted}. Finally, Section~\ref{s:conc} summarizes the topic and addresses open problems.

\section{Fundamentals of Thermoelectricity}\label{s:tebasics}
\subsection{Thermoelectricity in Bulk Materials}\label{s:tebasics:bulk}

\begin{figure}
	\centering
	\begin{tikzpicture}
		\fill[color=red,opacity=0.1] (0,0)-|(3,3)-|cycle;
		\fill[color=blue,opacity=0.1] (3,0)-|(6,3)-|cycle;
		\draw[thick] (0,0)-|(6,3)-|cycle;
		\draw (0,1.5)node[anchor=east]{$T_H$};
		\draw (6,1.5)node[anchor=west]{$T_C$};
		\draw[-latex,shift={(1,1)},rotate=180] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(1,0);
		\draw[-latex,shift={(2.3,0.6)},rotate=50] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(1,0);
		\draw[-latex,shift={(1.4,1.6)},rotate=0] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(1,0);
		\draw[-latex,shift={(0.4,2.5)},rotate=-50] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(1,0);
		\draw[-latex,shift={(2,2.6)},rotate=-100] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(1,0);
		\begin{scope}[shift={(3,-.5)}]
			\draw[-latex,shift={(1,1)},rotate=190] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(2.3,0.6)},rotate=50] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(1.4,1.6)},rotate=-10] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(0.4,2.5)},rotate=-830] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(2,2.6)},rotate=-120] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
		\end{scope}
		\begin{scope}[shift={(6,.4)},rotate=90]
			\draw[-latex,shift={(1,1)},rotate=190] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(2.3,0.6)},rotate=50] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(1.4,1.6)},rotate=-10] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(0.4,2.5)},rotate=-80] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
			\draw[-latex,shift={(2,2.6)},rotate=-120] (0,0)node[circle,fill=green,scale=0.5,draw=black]{}(0.1,0)--(0.5,0);
		\end{scope}
		\draw[thick,-latex](1,-.25)--(5,-.25)node[midway,anchor=north]{$\Delta V$};
	\end{tikzpicture}
	\caption{Electrons in a piece of metal with a temperature gradient in equilibrium.}
	\label{fig:metal_diff}
\end{figure}

First, the Seebeck effect where heat is converted to electric power, is introduced informally for bulk materials in order to form a basis for understanding thermoelectricity in graphene superlattices.\\

In simple bulk materials, thermoelectricity arises from the very general mechanism of diffusion. Diffusion happens whenever there are particles moving around randomly, e.g., a droplet of color in clear water or electrons in a (semi)conductor. Whenever there is a region with a higher concentration of particles and an adjacent region with a lower concentration, the random movement of the particles results in a net flow from the first area to the latter. Alternatively, whenever there is an equal concentration of particles in both materials but in one area they can move faster or more easily than in the other, there also is a net flow of particles from the first region to the latter.\\

The second observation can be used to explain the thermoelectric effect: Figure~\ref{fig:metal_diff} shows a piece of metal that has a hot (red) and a cold (blue) region, and electrons (green) inside the metal with the arrow representing their velocity. Suppose initially the electron concentration is equal everywhere. Then a temperature difference is applied resulting in a net flow of electrons to the right, since the electrons in the hot region move faster than those in the cold region. This continues until diffusion is balanced with electron drift due to the electric field generated by the unequal distribution of charges (left side is net positive while right side is net negative) which results in a voltage drop $\Delta V$. This balanced equilibrium state is precisely what is shown in Figure~\ref{fig:metal_diff}: The diffusion process gets weaker as the asymmetry in electron density starts to counter the asymmetry in movement speed, while the electric field which pushes electrons back to the left gets stronger with the asymmetry in the electron density.
The following relation can be observed:
\begin{equation}
	\Delta V = -S(T_H-T_C) = -S\Delta T,
\end{equation}
where $S$ is the so called \textit{Seebeck coefficient} or the thermopower. In this case $S$ is negative but if the charge carriers were positive in the same situation, $S$ would be positive.\\

However, this resulting voltage $\Delta V$ cannot be measured directly: If the hot and the cold side were connected to a voltmeter using a wire made of the same material as the sample, the temperature gradient that also occurs in the wire generates an equal and (in the loop we want to measure the voltage in) opposite Seebeck effect. This results in no voltage being measured. In order to measure a significant voltage or to generate electric power, a couple of two materials with Seebeck coefficients $S_1$ and $S_2$ is required such that the magnitude $|S|$ of the relative Seebeck coefficient $S=S_1-S_2$ is as large as possible. This is where semiconductors are very useful. Not only do they have very large Seebeck coefficients compared to metals, but both negative and positive coefficients are easily realized using n-doped (electrons as negative carriers) and p-doped (holes as positive carriers) materials, respectively, resulting in very large $|S|$.\\


To summarize the key point of this section: For a strong thermoelectric effect, the difference in ''mobility''\footnote{Note that ''mobility'' is quoted, since it refers to the general concept of a particle being mobile and not specifically to the charge carrier mobility $\mu=\frac{v_d}{{\mathcal{E}}}$ relating the drift velocity $v_d$ to strength of the electric field ${\mathcal{E}}$.} of charge carriers on the cold and hot side must be as large as possible. This translates to a large difference in ''mobility'' for energies below and above the so called \textit{Fermi energy} $E_F$. The reason for this is that for increasing temperature the number of electrons with energies $E>E_F$ increases while the number of electrons with $E<E_F$ decreases.\\

\subsection{Thermoelectric Figure of Merit}\label{s:tebasics:fom}

The quality of a material or couple is usually described by the dimensionless thermoelectric figure of merit
\begin{equation}
	ZT = \frac{\sigma S^2}{\kappa}T,
\end{equation}
where $\sigma$ is the electrical conductivity, $\kappa$ the heat conductivity, $S$ the Seebeck coefficient of the material or couple and $T$ the average temperature. In case of a couple, an average of the conductivities and the relative Seebeck coefficient are considered.
It is linked to the maximum efficiency 
\begin{equation}
	\eta_{\text{max}}=\frac{T_H-T_C}{T_H}\frac{\sqrt{1+ZT}-1}{\sqrt{1+ZT}+\frac{T_C}{T_H}}.\label{eq:efficiency}
\end{equation}
For large $ZT$, $\eta_{\text{max}}$ approaches the \textit{Carnot efficiency} $\frac{T_H-T_C}{T_H}$ which is the upper limit for any heat engine in general.

Next, the factors of $ZT$ (excluding $T$) are examined: Since the maximum output power of any generator is $P_{\text{max}}\propto\frac{V^2}{R}$, a large open circuit voltage $V$ and small internal resistance $R$ is desirable. In a thermoelectric generator, the first is achieved by large $S$ (or $\Delta T$) and the second by a high electrical conductivity $\sigma$. Additionally, a small heat conductivity $\kappa$ results in low thermal losses.

The optimization of those three factors is rather difficult, since improving one of them usually impairs the others. Copper for example is a good electrical conductor but also a good heat conductor. On top of that, its Seebeck coefficient is rather low at only $6.5\frac{\mu {\text{V}}}{{\text{K}}}$. Semiconductors on the other hand are - as the name implies - worse electrical conductors than metal, but their Seebeck coefficient is way larger. This issue can be encountered with nanostructured materials, e.g., by trapping phonons (vibrations in the material that carry a lot of heat), or by increasing the Seebeck coefficient to unprecedented values.\\

\section{Thermoelectric Devices Based on Graphene Superlattices}\label{s:gslted}

Graphene is a two-dimensional grid of carbon atoms arranged in a hexagonal pattern. It has been described as the ''ultimate flatland'' (see \cite{geim_flatland}), since no other known material can confine electrons to two dimensions that well (note that a single layer of graphene will be referred to as ''graphene'', in literature this is sometimes specified as mono-layer graphene). Together with the lattice symmetry of graphene this gives rise to some interesting properties of the electrons inside (see \cite{sarma_graphene_basics}):

\begin{enumerate}
	\item  Electrons (at least at the energy levels of interest) behave like massless particles moving at the Fermi velocity $v_F$. This is due to the linear relation between energy $E$ and momentum $k$ shown in Figure~\ref{fig:graphene_properties:b}.\\
	
	\item Due to the symmetry of the sublattices $A$ and $B$ (see Figure~\ref{fig:graphene_properties:a}) their quantum wavefunction has a spinor component that is similar but unrelated to the spin quantum number. It is called \textit{pseudospin} or \textit{chirality} and is conserved over time. This means that a particle can never change from the red to the blue branch in Figure~\ref{fig:graphene_properties:b} or vice versa.\\
\end{enumerate}

\begin{figure}
	\centering
	\begin{subfigure}{\linewidth}
		\centering
		\begin{tikzpicture}
		
		
		\draw (0,0)--(0,1)--({sqrt(3)/2},1.5)--({sqrt(3)},1)--({sqrt(3)},0)--({sqrt(3)/2},-0.5);
		\draw[rotate=120] (0,0)--(0,1)--({sqrt(3)/2},1.5)--({sqrt(3)},1)--({sqrt(3)},0)--({sqrt(3)/2},-0.5);
		\draw[rotate=240] (0,0)--(0,1)--({sqrt(3)/2},1.5)--({sqrt(3)},1)--({sqrt(3)},0)--({sqrt(3)/2},-0.5);
		
		\draw[thick,-latex](0.15,0)--({sqrt(3)-0.15},0)node[midway,anchor=south]{$\boldsymbol{a}_1$};
		\draw[thick,-latex,rotate=60](0.15,0)--({sqrt(3)-0.15},0)node[midway,anchor=west]{$\boldsymbol{a}_2$};
		\draw[thick,-latex](0,0.15)--(0,0.85)node[midway,anchor=east]{$\boldsymbol{\delta}$};
		
		\draw (0,0)node[circle,fill=blue,draw=black,radius=0.2]{};
		
		\draw (0,1)node[circle,fill=yellow,draw=black,radius=0.2]{};
		\draw ({sqrt(3)},0)node[circle,fill=blue,draw=black,radius=0.2]{};
		\draw ({sqrt(3)/2},1.5)node[circle,fill=blue,draw=black,radius=0.2]{};
		\draw (0,-2)node[circle,fill=yellow,draw=black,radius=0.2]{};
		
		
		\begin{scope}[rotate=120]
		\draw (0,1)node[circle,fill=yellow,draw=black,radius=0.2]{};
		\draw ({sqrt(3)},0)node[circle,fill=blue,draw=black,radius=0.2]{};
		\draw (0,-2)node[circle,fill=yellow,draw=black,radius=0.2]{};
		\draw ({sqrt(3)/2},1.5)node[circle,fill=blue,draw=black,radius=0.2]{};
		\end{scope}
		\begin{scope}[rotate=240]
		\draw (0,1)node[circle,fill=yellow,draw=black,radius=0.2]{};
		\draw ({sqrt(3)},0)node[circle,fill=blue,draw=black,radius=0.2]{};
		\draw (0,-2)node[circle,fill=yellow,draw=black,radius=0.2]{};
		\draw ({sqrt(3)/2},1.5)node[circle,fill=blue,draw=black,radius=0.2]{};
		\end{scope}
		
		\end{tikzpicture}
		\caption{Structure of graphene with two identical triangular sublattices $A$ (blue) and $B$ (yellow), their lattice vectors ${\boldsymbol{a}_1}$, ${\boldsymbol{a}}_2$, and the vector ${\boldsymbol{\delta}}$ from $A$ to $B$.}
		\label{fig:graphene_properties:a}
	\end{subfigure}
	\begin{subfigure}{\linewidth}
		\centering
		\begin{tikzpicture}
		\draw[-latex](-3,0)--(3,0)node[anchor=south]{$k$};
		\draw[-latex,rotate=90](-2,0)--(2,0)node[anchor=west]{$E$};
		\draw[thick,blue](-2,-2)--(2,2)(1.5,1)node{$+\sigma$};
		\draw[thick,red](-2,2)--(2,-2)(-1.5,1)node{$-\sigma$};
		\end{tikzpicture}
		\caption{Energy dispersion of low energy electrons in graphene, where $\pm\sigma$ represents positive/negative pseudospin.}
		\label{fig:graphene_properties:b}
	\end{subfigure}
	\caption{Properties of graphene, adapted from \cite{sarma_graphene_basics}}
	\label{fig:graphene_properties}
\end{figure}

\subsection{Resonant Tunnelling}\label{s:gslted:restun}

The resonant tunneling mechanism is based on the previously mentioned Klein paradox: Suppose an electron moves straight (at an incident angle of $\phi=0$) towards a very high potential barrier. Classically one would expect the electron to always be reflected and even considering quantum tunneling the electron should be reflected in most cases (the tunneling probability decreases exponentially with increasing potential or width of the barrier). In graphene, however, the electron is always transmitted through and never reflected by the barrier. This can be explained by the conservation of the pseudospin quantum number. The pseudospin of the reflected electron would have to be the exact opposite of the pseudospin of the incident electron which is impossible. Thus, the electron must always be transmitted.\\

In the general case where the electron arrives at an arbitrary angle $\phi$, the transmission probability in case of a very high potential barrier of width $d$ is given as 
\begin{equation}
	P_t = \frac{\cos^2(\phi)}{1-\cos^2(k'd)\sin^2(\phi)},\label{eq:transmittance}
\end{equation}
where $k'$ is the normal component (along the direction of $d$) of the momentum inside the barrier and depends on the energy $E$ of the electron and the incident angle $\phi$ (see \cite{katsnelson_tunnelling_klein}). Notably, if $k'd$ is an integer multiple of $\pi$, the transmittance $P_t$ equals $1$ regardless of $\phi$. This is called resonant tunneling.\\

Earlier, it was illustrated that the thermoelectric effect arises from an asymmetry in charge carrier transport around the Fermi energy $E_F$ between higher and lower energy particles, since the number of particles with energy $E>E_F$ increases at higher temperature while the number of particles with lower energy decreases.
According to \cite{dragoman_main}, the Seebeck coefficient quantum devices is given as
\begin{equation}
	S = \frac{\pi^2 k_B^2 T}{3e}\frac{{\text{d}}(\ln P_t)}{{\text{d}}E}\Big|_{E=E_F}. \label{eq:seebeck_quantum}
\end{equation}
So in order to achieve large $|S|$, $P_t$ over $E$ should feature very sharp edges that are asymmetric around $E_F$. While the resonant tunneling mechanism with one barrier results in an energy dependent transmittance, $P_t$ in Eq.~\eqref{eq:transmittance} is a rather smooth function of energy $E$.

\subsection{Thermoelectricity in Graphene Superlattices}

\begin{figure}
	\begin{subfigure}{\linewidth}		
		\centering
		\includegraphics[width=\linewidth]{figures/dragoman_device}
		\caption{Structure of the device.}
		\vspace{.5cm}
		\label{fig:dragoman:device}
	\end{subfigure}
	\begin{subfigure}{\linewidth}		
		\centering
		\includegraphics[width=\linewidth]{figures/dragoman_k_T_lattice}
		\caption{Transmission probability over momentum at $V_G=0.8{\text{V}}$ and $\phi=10^{\text{o}}$. Dashed and solid lines represent $m=10$ and $m=25$, respectively.}
		\vspace{.5cm}
		\label{fig:dragoman:k_T}
	\end{subfigure}
	\begin{subfigure}{\linewidth}		
		\centering
		\includegraphics[width=\linewidth]{figures/dragoman_VG_S}
		\caption{Seebeck coefficient over electrode potential at $k=\frac{2\pi}{50{\text{nm}}}$, $\phi=15^{\text{o}}$, and $m=25$.}
		\label{fig:dragoman:VG_S}
	\end{subfigure}
	\caption{TED based on graphene superlattices of Dragoman \textit{et al.} in \cite{dragoman_main} with its characteristics.}
	\label{fig:dragoman}
\end{figure}

In order to achieve sharper edges in transmission probability, graphene superlattices in which a potential pattern repeats itself a certain number of times can be used. Superlattices are usually denoted as $(XY)^m$ where a strip of $X$ (some type of graphene) is followed by a strip of $Y$ and that pattern repeats itself $m$ times. For example the structure in Figure~\ref{fig:dragoman:device} is a $(0P)^70$ superlattice, where $0$ represents plain graphene and $P$ represents strips where a potential is applied. In \cite{dragoman_main}, Dragoman \textit{et al.} derive the transmission probability of an electron through the device in Figure~\ref{fig:dragoman:device}, depending on the superlattice, the applied potential $V_G$, the incident angle $\phi$, and the electron energy $E$. To summarize their key observations, for a larger lattice, i.e, larger $m$ in $(0P)^m0$, the transmission characteristic gets sharper. Furthermore, the transmission characteristic features a ''bandgap'' inside of which electrons are not transmitted (see Figure~\ref{fig:dragoman:k_T}). The electrode potential $V_G$ can be used to tune the Seebeck coefficient, enabling the device to reach values of $|S|>30\frac{{\text{mV}}}{{\text{K}}}$.\\

This value is still surpassed in the research of Mishra \textit{et al.} in \cite{mishra_main}. They extend the graphene superlattice to an $(NP)^{m/2}D(NP)^{m/2}$ heterostructure, where $N$ and $P$ are n-doped and p-doped (by positive and negative external potentials $V_N$ and $V_P$) strips respectively. The strip $D$ has a potential $V_D$ that differs from $V_N$ and $V_P$. This causes a very sharp peak to appear inside the gap of the transmission characteristic. With an increasing period number $m$, the peak gets sharper and smaller, resulting in a very large Seebeck coefficient: For potentials $V_N=-V_P=20{\text{mV}}$, $V_D=0{\text{mV}}$, strip widths $W=20{\text{nm}}$, incident angle $\phi=10^{\text{o}}$, and period number $m=50$ it reaches a maximum of $1.2\frac{{\text{V}}}{{\text{K}}}$ - almost six orders of magnitude larger than the Seebeck coefficient in copper. By varying $V_N$, for example, the peak Seebeck coefficient can be shifted to occur at different electron energies.\\

%Figures of Mishra \textit{et al.} \cite{mishra_main} are not included in this paper, since they are very detailed and beyond the scope here. They are, however, worth having a look at, since they visualize the effect of the different parameters on the Seebeck coefficient very clearly.\\

\subsection{Caveats}

Although the large value of $|S|$ in superlattice heterostructures should easily compensate for the high thermal conductivity $\kappa$ in $ZT=\frac{S^2\sigma}{\kappa}T$, other problems arise with such devices, mainly arising from the 2D nature of graphene. First of all, it is inevitable that heat inside the graphene will flow into the third dimension of space, e.g., into the substrate on which the graphene is placed. This results in additional thermal losses. Another challenge is the achievement of proper thermal coupling to the graphene layer, since the total thermal conductivity is proportional to the cross-sectional area of a conductor which is in the case of graphene - being a 2D material - approximately $0$.

\section{Conclusion}\label{s:conc}

Graphene superlattices, especially heterostructures, achieve unprecedented values in their Seebeck coefficient, making them a very promising candidate for $ZT\gtrsim3$ thermoelectric devices. In the future they could thus be used to replace current means of refrigeration and electric power generation.\\

First however, a thorough analysis of the electrical and thermal conductivity of the superlattice heterostructure is required to make a statement about the actual efficiency of such devices. Achieving proper thermal coupling and minimizing heat losses due to the 2D nature of graphene are also open issues that require further research.\\

\bibliographystyle{IEEEtran}
\bibliography{literatureProject12}


\end{document}
